message("UNIX non APPLE environment")
message("Please run the configuration like this:")
message("cmake -G \"Unix Makefiles\" -DCMAKE_BUILD_TYPE=Debug -DBUILD_USER_MANUAL=1 ../development")
message("If using the locally built pappsomspp libs, add -DLOCAL_DEV=1")

set(CMAKE_C_IMPLICIT_INCLUDE_DIRECTORIES /usr/include)
set(CMAKE_CXX_IMPLICIT_INCLUDE_DIRECTORIES /usr/include)

# This is used throughout all the build system files
set(TARGET ${LOWCASE_PROJECT_NAME})

find_package(Qt6 COMPONENTS Widgets Xml Svg PrintSupport Network GLOBAL REQUIRED)

find_package(IsoSpec++ GLOBAL REQUIRED)

find_package(QCustomPlotQt6 GLOBAL REQUIRED)
# Per instructions of the lib author:
# https://www.qcustomplot.com/index.php/tutorials/settingup
message(STATUS "Setting definition -DQCUSTOMPLOT_USE_LIBRARY.")
message(STATUS "QCustomPlotQt6_LIBRARIES: ${QCustomPlotQt6_LIBRARIES}")
message(STATUS "QCustomPlotQt6_INCLUDE_DIR: ${QCustomPlotQt6_INCLUDE_DIR}")

find_package(QuaZip-Qt6 GLOBAL REQUIRED)

message("")
message("${BoldGreen}BEGIN USING LOCAL DEVELOPMENT LIBS FOR PAPPSOMSPP${ColourReset}")
message("")

# In this file we handle the case when developing both libs and this program, we
# need to be able to load the libs from their dev/build directory without
# relying on the system libs. This CMake file is automatically included if
# -DLOCAL_DEV=1 is added to CMake configuration command line.

# The development should use the locally built libs.
set(PappsoMSpp_FOUND 1)
set(PappsoMSpp_INCLUDE_DIRS "$ENV{HOME}/devel/pappsomspp/development/src")
set(PappsoMSpp_LIBRARIES
  "$ENV{HOME}/devel/pappsomspp/build-area/unix/src/libpappsomspp.so.0") 
if(NOT TARGET PappsoMSpp::Core)

  add_library(PappsoMSpp::Core UNKNOWN IMPORTED GLOBAL)
  set_target_properties(PappsoMSpp::Core PROPERTIES
    IMPORTED_LOCATION ${PappsoMSpp_LIBRARIES}
    INTERFACE_INCLUDE_DIRECTORIES ${PappsoMSpp_INCLUDE_DIRS})

endif()

set(PappsoMSppWidget_FOUND 1)
set(PappsoMSppWidget_LIBRARIES
  "$ENV{HOME}/devel/pappsomspp/build-area/unix/src/pappsomspp/widget/libpappsomspp-widget.so.0") 
if(NOT TARGET PappsoMSpp::Widget)

  add_library(PappsoMSpp::Widget UNKNOWN IMPORTED GLOBAL)
  set_target_properties(PappsoMSpp::Widget PROPERTIES
    IMPORTED_LOCATION ${PappsoMSppWidget_LIBRARIES}
    INTERFACE_INCLUDE_DIRECTORIES ${PappsoMSpp_INCLUDE_DIRS})

endif()

include_directories(${include_directories} ${PappsoMSpp_INCLUDE_DIRS} ${PappsoMSpp_INCLUDE_DIRS})

message("")
message("${BoldGreen}DONE USING LOCAL DEVELOPMENT LIBS FOR PAPPSOMSPP${ColourReset}")
message("")

# Find the libmass static library
set(libmass_FOUND 1)
# Below: for the generated UIS_H files and also to access the includes like
# #include <libmass/_file_> or #include <_file_>
set(libmass_INCLUDE_DIRS "${CMAKE_SOURCE_DIR}/libmass/includes"
  "${CMAKE_SOURCE_DIR}/libmass/includes/libmass" "${CMAKE_BINARY_DIR}/libmass")
set(libmass_LIBRARIES "${CMAKE_SOURCE_DIR}/libmass/libmass.a") 
if(NOT TARGET libmass::nongui)

  add_library(libmass::nongui UNKNOWN IMPORTED)

  set_target_properties(libmass::nongui PROPERTIES
    IMPORTED_LOCATION             "${libmass_LIBRARIES}"
    INTERFACE_INCLUDE_DIRECTORIES "${libmass_INCLUDE_DIRS}"
  )
endif()

message(STATUS "Found libmass at: ${libmass_LIBRARIES} with include dir: ${libmass_INCLUDE_DIRS}")

# Find the libmassgui static library
set(libmassgui_FOUND 1)
# Below: for the generated UIS_H files and also to access the includes like
# #include <libmassgui/_file_> or #include <_file_>
set(libmassgui_INCLUDE_DIRS "${CMAKE_SOURCE_DIR}/libmassgui/includes"
  "${CMAKE_SOURCE_DIR}/libmassgui/includes/libmassgui" "${CMAKE_BINARY_DIR}/libmassgui")
set(libmassgui_LIBRARIES "${CMAKE_SOURCE_DIR}/libmassgui/libmassgui.a") 
if(NOT TARGET libmass::gui)

  add_library(libmass::gui UNKNOWN IMPORTED)

  set_target_properties(libmass::gui PROPERTIES
    IMPORTED_LOCATION             "${libmassgui_LIBRARIES}"
    INTERFACE_INCLUDE_DIRECTORIES "${libmassgui_INCLUDE_DIRS}"
  )
endif()

message(STATUS "Found libmassgui at: ${libmassgui_LIBRARIES} with include dir: ${libmassgui_INCLUDE_DIRS}")


## Install directories
set(BIN_DIR ${CMAKE_INSTALL_PREFIX}/bin)
set(DOC_DIR ${CMAKE_INSTALL_PREFIX}/share/doc/${TARGET})


# The appstream, desktop and icon files
install(FILES org.msxpertsuite.${TARGET}.desktop
  DESTINATION ${CMAKE_INSTALL_PREFIX}/share/applications)

install(FILES org.msxpertsuite.${TARGET}.appdata.xml
  DESTINATION ${CMAKE_INSTALL_PREFIX}/share/metainfo)

install(FILES images/icons/16x16/${TARGET}.png
  DESTINATION
  ${CMAKE_INSTALL_PREFIX}/share/icons/hicolor/16x16/apps)

install(FILES images/icons/32x32/${TARGET}.png
  DESTINATION
  ${CMAKE_INSTALL_PREFIX}/share/icons/hicolor/32x32/apps)

install(FILES images/icons/48x48/${TARGET}.png
  DESTINATION
  ${CMAKE_INSTALL_PREFIX}/share/icons/hicolor/48x48/apps)

install(FILES images/icons/64x64/${TARGET}.png
  DESTINATION
  ${CMAKE_INSTALL_PREFIX}/share/icons/hicolor/64x64/apps)


# Prepare the AppImage recipe file to be used elsewhere, 
# for deploying mineXpert2 on non-Debian- or non-Ubuntu platforms.

configure_file(${CMAKE_SOURCE_DIR}/CMakeStuff/${CMAKE_PROJECT_NAME}-appimage-recipe.yml.in
  ${CMAKE_SOURCE_DIR}/appimage/${CMAKE_PROJECT_NAME}-appimage-recipe.yml @ONLY)

## Platform-dependent compiler flags:
include(CheckCXXCompilerFlag)

if (WITH_FPIC)
  add_definitions(-fPIC)
endif()

