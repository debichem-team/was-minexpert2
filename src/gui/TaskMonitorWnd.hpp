
/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2019 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */

#pragma once


/////////////////////// StdLib includes


/////////////////////// Qt includes
#include <QMainWindow>
#include <QScrollArea>


/////////////////////// pappsomspp includes


/////////////////////// Local includes
#include "ui_TaskMonitorWnd.h"
#include "TaskMonitorCompositeWidget.hpp"


namespace msxps
{
namespace minexpert
{


class TaskMonitorWnd : public QMainWindow
{
  Q_OBJECT

  public:
  // Construction/destruction
  TaskMonitorWnd(QWidget *parent,
                 const QString &title,
                 const QString &settingsTitle,
                 const QString &description = QString());
  virtual ~TaskMonitorWnd();

  virtual void writeSettings();
  virtual void readSettings();

  virtual bool initialize();
  virtual void closeEvent(QCloseEvent *event);

  virtual void show();

  TaskMonitorCompositeWidget *addTaskMonitorWidget(const QColor &color);

  protected:
  ////// Setting up the window layout.

  //! Graphical interface definition.
  Ui::TaskMonitorWnd m_ui;

  private:
  QString m_title;
  QString m_settingsTitle;
  QString m_description;
};


} // namespace minexpert

} // namespace msxps
