/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright (C) 2009--2020 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once

#include <vector>

/////////////////////// Qt includes
#include <QSortFilterProxyModel>
#include <QFormLayout>
#include <QDialog>


/////////////////////// Local includes
#include "BasePlotWnd.hpp"
#include "ui_ProcessingFlowViewerDlg.h"


namespace msxps
{
namespace minexpert
{


class ProgramWindow;

class ProcessingFlowViewerDlg : public QDialog
{
  Q_OBJECT

  public:
  ProcessingFlowViewerDlg(BasePlotWnd *base_plot_wnd_p, const QString &applicationName);

  virtual ~ProcessingFlowViewerDlg();

	void setProcessingFlowText(const QString &processing_flow_text);

  public slots:

  private:
  Ui::ProcessingFlowViewerDlg m_ui;

  QString m_applicationName;
  QString m_fileName;

  ProgramWindow *mp_programWindow = nullptr;

  Q_INVOKABLE void writeSettings();
  Q_INVOKABLE void readSettings();

  void closeEvent(QCloseEvent *event);

  bool setupDialog();
};

} // namespace minexpert

} // namespace msxps

