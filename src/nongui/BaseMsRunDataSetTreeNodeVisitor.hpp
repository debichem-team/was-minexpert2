/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2019 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */

#pragma once

/////////////////////// StdLib includes


/////////////////////// Qt includes


/////////////////////// pappsomspp includes
#include <pappsomspp/massspectrum/qualifiedmassspectrum.h>
#include <pappsomspp/msrun/msrundatasettreenode.h>
#include <pappsomspp/msrun/msrundatasettreevisitor.h>


/////////////////////// Local includes
#include "ProcessingFlow.hpp"
#include "MsRunDataSet.hpp"


namespace msxps
{
namespace minexpert
{


class BaseMsRunDataSetTreeNodeVisitor;

class MassDataIntegrator;

typedef std::shared_ptr<BaseMsRunDataSetTreeNodeVisitor>
  BaseMsRunDataSetTreeNodeVisitorSPtr;
typedef std::shared_ptr<const BaseMsRunDataSetTreeNodeVisitor>
  BaseMsRunDataSetTreeNodeVisitorCstSPtr;

class BaseMsRunDataSetTreeNodeVisitor
  : public QObject,
    public pappso::MsRunDataSetTreeNodeVisitorInterface
{
  Q_OBJECT

  friend class MassDataIntegrator;

  public:
  BaseMsRunDataSetTreeNodeVisitor(MsRunDataSetCstSPtr ms_run_data_set_csp,
                                  const ProcessingFlow &processing_flow);

  BaseMsRunDataSetTreeNodeVisitor(const BaseMsRunDataSetTreeNodeVisitor &other);

  virtual ~BaseMsRunDataSetTreeNodeVisitor();

  void setProcessingFlow(const ProcessingFlow &processing_flow);

  BaseMsRunDataSetTreeNodeVisitor &
  operator=(const BaseMsRunDataSetTreeNodeVisitor &other);

  virtual pappso::QualifiedMassSpectrumCstSPtr
  checkQualifiedMassSpectrum(const pappso::MsRunDataSetTreeNode &node);

  virtual void setNodesToProcessCount(std::size_t nodes_to_process);
  virtual bool shouldStop() const;
  virtual void silenceFeedback(bool silence_feedback = false);
  virtual bool shouldSilenceFeedback();

  public slots:
  virtual void cancelOperation();

  signals:
  void cancelOperationSignal();

  void setProgressBarMaxValueSignal(std::size_t nodes_to_process);
  void incrementProgressBarMaxValueSignal(std::size_t increment);
  void setProgressBarCurrentValueSignal(std::size_t processed_nodes);
  void
  incrementProgressBarCurrentValueAndSetStatusTextSignal(std::size_t increment,
                                                         QString text);

  void setStatusTextSignal(QString text);

  void setStatusTextAndCurrentValueSignal(QString text, std::size_t value);
  void setStatusTextAndIncrementCurrentValueSignal(QString text,
                                                   std::size_t increment);

  protected:
  MsRunDataSetCstSPtr mcsp_msRunDataSet;
  ProcessingFlow m_processingFlow;

  std::pair<double, double> m_dtRange =
    std::pair<double, double>(std::numeric_limits<double>::quiet_NaN(),
                              std::numeric_limits<double>::quiet_NaN());
  std::pair<double, double> m_mzRange =
    std::pair<double, double>(std::numeric_limits<double>::quiet_NaN(),
                              std::numeric_limits<double>::quiet_NaN());
  std::pair<double, double> m_rtRange =
    std::pair<double, double>(std::numeric_limits<double>::quiet_NaN(),
                              std::numeric_limits<double>::quiet_NaN());

  virtual void setInnermostRanges();

  virtual bool checkMsLevel(pappso::QualifiedMassSpectrumCstSPtr &qualified_mass_spectrum_csp);

  virtual bool checkRtRange(
    pappso::QualifiedMassSpectrumCstSPtr &qualified_mass_spectrum_csp);

  virtual bool checkDtRange(
    pappso::QualifiedMassSpectrumCstSPtr &qualified_mass_spectrum_csp);

  bool m_isOperationCancelled       = false;
  bool m_isProgressFeedbackSilenced = false;
};


} // namespace minexpert

} // namespace msxps
