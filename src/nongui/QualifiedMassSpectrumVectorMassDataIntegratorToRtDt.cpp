/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright (C) 2009--2020 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// StdLib includes
#include <iostream>
#include <iomanip>
#include <cmath>


/////////////////////// OpenMP include
#include <omp.h>

/////////////////////// Qt includes
#include <QDebug>
#include <QThread>

/////////////////////// pappsomspp includes
#include <pappsomspp/processing/combiners/tracepluscombiner.h>


/////////////////////// Local includes
#include "QualifiedMassSpectrumVectorMassDataIntegratorToRtDt.hpp"
#include "ProcessingStep.hpp"
#include "ProcessingType.hpp"


int qualifiedMassSpectrumVectorMassDataIntegratorToRtDtMetaTypeId =
  qRegisterMetaType<
    msxps::minexpert::QualifiedMassSpectrumVectorMassDataIntegratorToRtDt>(
    "msxps::minexpert::QualifiedMassSpectrumVectorMassDataIntegratorToRtDt");

int qualifiedMassSpectrumVectorMassDataIntegratorToRtDtSPtrMetaTypeId =
  qRegisterMetaType<
    msxps::minexpert::QualifiedMassSpectrumVectorMassDataIntegratorToRtDtSPtr>(
    "msxps::minexpert::"
    "QualifiedMassSpectrumVectorMassDataIntegratorToRtDtSPtr");


namespace msxps
{
namespace minexpert
{


// Needed for qRegisterMetaType
QualifiedMassSpectrumVectorMassDataIntegratorToRtDt::
  QualifiedMassSpectrumVectorMassDataIntegratorToRtDt()
  : QualifiedMassSpectrumVectorMassDataIntegrator()
{
}


QualifiedMassSpectrumVectorMassDataIntegratorToRtDt::
  QualifiedMassSpectrumVectorMassDataIntegratorToRtDt(
    MsRunDataSetCstSPtr ms_run_data_set_csp,
    const ProcessingFlow &processing_flow,
    QualifiedMassSpectraVectorSPtr &qualified_mass_spectra_to_integrate_sp)
  : QualifiedMassSpectrumVectorMassDataIntegrator(
      ms_run_data_set_csp,
      processing_flow,
      qualified_mass_spectra_to_integrate_sp)
{
  m_processingFlow.setMsRunDataSetCstSPtr(mcsp_msRunDataSet);
}


// Needed for qRegisterMetaType
QualifiedMassSpectrumVectorMassDataIntegratorToRtDt::
  QualifiedMassSpectrumVectorMassDataIntegratorToRtDt(
    const QualifiedMassSpectrumVectorMassDataIntegratorToRtDt &other)
  : QualifiedMassSpectrumVectorMassDataIntegrator(other)
{
}


QualifiedMassSpectrumVectorMassDataIntegratorToRtDt::
  ~QualifiedMassSpectrumVectorMassDataIntegratorToRtDt()
{
}


std::size_t
QualifiedMassSpectrumVectorMassDataIntegratorToRtDt::getColorMapKeyCellCount()
  const
{
  return m_colorMapKeyCellCount;
}


std::size_t
QualifiedMassSpectrumVectorMassDataIntegratorToRtDt::getColorMapMzCellCount()
  const
{
  return m_colorMapMzCellCount;
}


double
QualifiedMassSpectrumVectorMassDataIntegratorToRtDt::getColorMapMinKey() const
{
  return m_colorMapMinKey;
}


double
QualifiedMassSpectrumVectorMassDataIntegratorToRtDt::getColorMapMaxKey() const
{
  return m_colorMapMaxKey;
}


double
QualifiedMassSpectrumVectorMassDataIntegratorToRtDt::getColorMapMinMz() const
{
  return m_colorMapMinMz;
}


double
QualifiedMassSpectrumVectorMassDataIntegratorToRtDt::getColorMapMaxMz() const
{
  return m_colorMapMaxMz;
}


using Map = std::map<double, pappso::MapTrace>;
const std::shared_ptr<Map> &
QualifiedMassSpectrumVectorMassDataIntegratorToRtDt::getRtMapTraceMapSPtr()
  const
{
  return m_rtMapTraceMapSPtr;
}

pappso::DataKind
QualifiedMassSpectrumVectorMassDataIntegratorToRtDt::
  getLastIntegrationDataKind() const
{
  return m_lastIntegrationDataKind;
}


void
QualifiedMassSpectrumVectorMassDataIntegratorToRtDt::integrate()
{
  qDebug();

  std::chrono::system_clock::time_point chrono_start_time =
    std::chrono::system_clock::now();

  // If there are no data, nothing to do.
  std::size_t mass_spectra_count =
    mcsp_qualifiedMassSpectraToIntegrateVector->size();

  if(!mass_spectra_count)
    {
      qDebug() << "The qualified mass spectrum vector is empty, nothing to do.";

      emit cancelOperationSignal();
    }

  qDebug() << "The number of mass spectra to handle:" << mass_spectra_count;

  // We need to clear the map trace!
  m_mapTrace.clear();

  emit setTaskDescriptionTextSignal(
    "Integrating to a TIC|XIC chromatogram / drift spectrum color map");

  // In this integration, we are going to craft the data to create a colormap
  // that will plot dt data against rt data or vice versa.

  if(!m_processingFlow.size())
    qFatal("The processing flow cannot be empty. Program aborted.");

  // When constructing this integrator, the processing flow passed as parameter
  // was used to gather innermost ranges data. When the steps that match
  // the innermost criterion, these are stored in m_innermostSteps2D. When the
  // steps are non-2D steps, then only the m_xxRange values are updated.

  // If the m_innermostSteps2D vector of ProcessingSteps is non-empty, then,
  // that means that we may have some MZ-related 2D processing steps to account
  // at combination time, because that is precisely the only moment that we can
  // do that work. To be able to provide selection polygon an mass
  // spectrum-specific dt|rt data, we have the SelectionPolygonSpec class. There
  // can be as many such class instances as required to document the integration
  // boundaries rt/mz and dt/mz for example. This is why we need to fill in the
  // vector of SelectionPolygonSpec and then set that to the combiner.

  // Note, however, that the hassle described above only is acceptable when the
  // selection polygon is not square. If the selection polygon is square, then
  // the ranges suffice, and they have been filled-in properly upon construction
  // of this integrator.

  qDebug() << "Now filling-in the selection_polygon_specs.";

  std::vector<pappso::SelectionPolygonSpec> selection_polygon_specs;

  std::size_t selection_polygon_spec_count =
    fillInSelectionPolygonSpecs(selection_polygon_specs);

  qDebug() << selection_polygon_spec_count
           << "selection polygon specs were retained for the configuration of "
              "the filter.";

  // Using a shared pointer make it easy to handle the copying of the map to
  // users of that map without bothering.

  if(m_rtMapTraceMapSPtr == nullptr)
    m_rtMapTraceMapSPtr =
      std::make_shared<std::map<double, pappso::MapTrace>>();
  else
    m_rtMapTraceMapSPtr->clear();

  // In this function, rt is the x_axis and dt is the y_axis, for each (rt,dt)
  // pair, the TIC intensity is the color. So we have m_rtMapTraceMapSPtr =
  // <rt, <dt, int>>.

  // The logic here is that in IM-MS experiments, for a given retention time
  // (rt), the might be hundrds of drift time-resolvec mass spectra. So we
  // create a map that relates rt values with a MapTrace, itself relating dt
  // values (map key) and TICint values (map value). During iterating in all the
  // mass spectra, we look at the rt value of the mass spectrum, we then look at
  // its dt value. We compute a TICint of the mass spectrum that we set in the
  // MapTrace, associated with the dt value. That MapTrace is then set the
  // member map that maps the rt value (map key) with the MapTrace (map value).

  // We want to parallelize the computation. Se we will allocate a vector of
  // iterators (as many as possible for the available processor threads), each
  // iterator having a subset of the initial data to integrate.

  // Determine the best number of threads to use and how many spectra to provide
  // each of the threads.

  // In the pair below, first is the ideal number of threads and second is the
  // number of mass spectra per thread.
  std::pair<std::size_t, std::size_t> best_parallel_params =
    bestParallelIntegrationParams(
      mcsp_qualifiedMassSpectraToIntegrateVector->size());

  // Handy alias.
  using MassSpecVector = std::vector<pappso::QualifiedMassSpectrumCstSPtr>;
  using VectorIterator = MassSpecVector::const_iterator;

  // Prepare a set of iterators that will distribute all the mass spectra
  // uniformly.
  std::vector<std::pair<VectorIterator, VectorIterator>> iterators =
    calculateIteratorPairs(best_parallel_params.first,
                           best_parallel_params.second);

  // Set aside a vector of std::map<double, pappso::MapTrace> to store retention
  // time values and the combined mass spectrum.

  // Handy aliases.
  using Map     = std::map<double, pappso::MapTrace>;
  using MapSPtr = std::shared_ptr<Map>;

  std::vector<MapSPtr> vector_of_rt_maptrace_maps;
  for(std::size_t iter = 0; iter < iterators.size(); ++iter)
    vector_of_rt_maptrace_maps.push_back(std::make_shared<Map>());

  // Now that we have all the parallel material, we can start the parallel work.
  emit setProgressBarMaxValueSignal(mass_spectra_count);

  qDebug() << "Starting the iteration in the visitors.";

  omp_set_num_threads(iterators.size());
#pragma omp parallel for ordered
  for(std::size_t iter = 0; iter < iterators.size(); ++iter)
    {
      // qDebug() << "thread index:" << iter;

      // Get the iterator range that we need to deal in this thread.
      VectorIterator current_iterator = iterators.at(iter).first;
      VectorIterator end_iterator     = iterators.at(iter).second;

      // Get the map for this thread.

      MapSPtr iterated_rt_maptrace_map_sp = vector_of_rt_maptrace_maps.at(iter);

      while(current_iterator != end_iterator)
        {
          // qDebug() << "thread index:" << iter
          //<< "iterating in one mass spectrum.";

          // We want to be able to intercept any cancellation of the
          // operation.

          if(m_isOperationCancelled)
            break;

          // Make the computation.

          // For each mass spectrum all we need is compute its TIC value, that
          // is, the sum of all its intensities (the y value of the DataPoint
          // instances it contains).

          pappso::QualifiedMassSpectrumCstSPtr qualified_mass_spectrum_csp =
            *current_iterator;

          // Ensure that the qualified mass spectrum contains a mass spectrum
          // that effectively contains the binary (mz,i) data.

          qualified_mass_spectrum_csp =
            checkQualifiedMassSpectrum(qualified_mass_spectrum_csp);

          if(qualified_mass_spectrum_csp == nullptr)
            qFatal(
              "Programming error. Not possible that shared pointer to "
              "qualified mass spectrum be nullptr.");

          // Great, at this point we actually have the mass spectrum!

          // We now need to ensure that the processing flow allows for this mass
          // spectrum to be accounted for.

          // Immediately check if the qualified mass spectrum is of a MS level
          // that matches the greatest MS level found in the member processing
          // flow instance.

          if(!checkMsLevel(qualified_mass_spectrum_csp))
            {
              qDebug() << "The MsLevel check returned false";
              ++current_iterator;
              continue;
            }
          if(!checkMsFragmentation(qualified_mass_spectrum_csp))
            {
              qDebug() << "The MsFragmentation check returned false";
              ++current_iterator;
              continue;
            }

          // Make easy check about RT and DT.

          if(!checkRtRange(qualified_mass_spectrum_csp))
            {
              qDebug() << "checkRtRange failed check.";

              ++current_iterator;
              continue;
            }
          if(!checkDtRange(qualified_mass_spectrum_csp))
            {
              qDebug() << "checkDtRange failed check.";

              ++current_iterator;
              continue;
            }

          double dt_key = qualified_mass_spectrum_csp->getDtInMilliSeconds();
          double rt_key = qualified_mass_spectrum_csp->getRtInMinutes();

          double mass_spectrum_sum_y = 0;

          // We want to know what is the TIC intensity value of the spectrum.

          // At this point we need to account for any limitations in the m/z
          // dimension of the data. These limitations might be as easy as having
          // a single m/z range limit or as complex as having one or more 2D
          // steps involving the m/z dimension in the m_innermostSteps2D, if the
          // selection polygons are not rectangle.

          if(!selection_polygon_specs.size())
            {
              if(!std::isnan(m_mzRange.first) && !std::isnan(m_mzRange.second))
                {
                  mass_spectrum_sum_y =
                    qualified_mass_spectrum_csp->getMassSpectrumSPtr()->sumY(
                      m_mzRange.first, m_mzRange.second);
                }
              else
                {
                  mass_spectrum_sum_y =
                    qualified_mass_spectrum_csp->getMassSpectrumSPtr()->sumY();
                }
            }
          else
            {
              // It may be possible that the 2D steps limiting the m/z range
              // have been followed by 1D steps further limiting the m/Z range.
              // The point is, the 1D steps are not stored in
              // selection_polygon_specs. However, by necessity, if there is at
              // least one selection_polygon_spec, then the m_mzRange holds
              // values. Futher, if there had been a 1D step limiting even more
              // the m/z range, then that step would have been accounted for in
              // m_mzRange. So we can account that limit right away.

              if(std::isnan(m_mzRange.first) || std::isnan(m_mzRange.second))
                qFatal("Programming error.");

              pappso::Trace filtered_trace(
                *qualified_mass_spectrum_csp->getMassSpectrumSPtr());

              pappso::FilterResampleKeepXRange the_keep_filter(
                m_mzRange.first, m_mzRange.second);

              filtered_trace = the_keep_filter.filter(filtered_trace);

              pappso::FilterResampleKeepPointInPolygon the_polygon_filter(
                selection_polygon_specs);

              filtered_trace =
                the_polygon_filter.filter(filtered_trace, dt_key, rt_key);

              mass_spectrum_sum_y = filtered_trace.sumY();
            }

          if(!mass_spectrum_sum_y)
            {
              ++current_iterator;
              continue;
            }

          // Now look into the <double, MapTrace> map that relates rt values
          // with MapTraces, if we already encountered the rt_key value. Store
          // the found place in an iterator.

          Map::iterator double_maptrace_map_iterator;

          double_maptrace_map_iterator =
            iterated_rt_maptrace_map_sp->find(rt_key);

          // Tells if the key was already found in the map.
          if(double_maptrace_map_iterator != iterated_rt_maptrace_map_sp->end())
            {

              // The map already contained a pair that had the key value ==
              // rt_key (->first membero of the pair). The ->second member of
              // the pair is the MapTrace that maps <dt_key, TICint>. So we
              // now need to search for a map item that has key == dt_key.

              using MapIterator = std::map<double, double>::iterator;

              std::pair<MapIterator, bool> res =
                double_maptrace_map_iterator->second.insert(
                  std::pair<double, double>(dt_key, mass_spectrum_sum_y));

              if(!res.second)
                {
                  // One other same dt_key value was seen already. Only
                  // increment the y value.

                  res.first->second += mass_spectrum_sum_y;
                }
            }
          else
            {

              // This is the first time that we encounter dt_key. So we need
              // to create a new MapTrace for that dt_key. That MapTrace
              // (std::map<double,double>) will store the TIC values for all the
              // dt_key elements.

              // Instantiate a new MapTrace in which we'll store all the TIC
              // intensities for all the dt_key map items.

              pappso::MapTrace map_trace;

              map_trace.insert(
                std::pair<double, double>(dt_key, mass_spectrum_sum_y));

              // Finally, store in the map<double, MapTrace> map, the map_trace
              // along with its dt_key.

              (*iterated_rt_maptrace_map_sp)[rt_key] = map_trace;
            }

          // At this point we have processed the qualified mass spectrum.

          emit incrementProgressBarCurrentValueAndSetStatusTextSignal(
            1, "Processing spectra");

          // Now we can go to the next mass spectrum.
          ++current_iterator;
        }
      // End of
      // while(current_iterator != end_iterator)
    }
  // End of
  // #pragma omp parallel for ordered
  // for(std::size_t iter = 0; iter < iterators.size(); ++iter)

  // In the loop above, for each thread, a std::map<double, pappso::MapTrace>
  // map was filled in with the dt|rt, MapTrace pairs.

  // If the task was cancelled, the monitor widget was locked. We need to
  // unlock it.
  emit unlockTaskMonitorCompositeWidgetSignal();
  emit setupProgressBarSignal(0, vector_of_rt_maptrace_maps.size() - 1);

  // We might be here because the user cancelled the operation in the for loop
  // above (visiting all the visitors). In this case m_isOperationCancelled is
  // true. We want to set it back to false, so that the following loop is gone
  // through. The user can ask that the operation be cancelled once more. But
  // we want that at least the performed work be used to show the trace.

  m_isOperationCancelled = false;
  int iter               = 0;

  pappso::TracePlusCombiner combiner(0);

  for(auto &&map_sp : vector_of_rt_maptrace_maps)
    {
      // qDebug();

      ++iter;

      if(m_isOperationCancelled)
        break;

      for(auto &&pair : *map_sp)
        {
          // first is the dt_key
          // second is the pappso::MapTrace

          emit setStatusTextSignal(
            QString("Consolidating color map data from thread %1").arg(iter));

          emit setProgressBarCurrentValueSignal(iter);

          double dt_key = pair.first;

          pappso::MapTrace current_maptrace = pair.second;

          // Now search in the member if a dt_key was found already.

          Map::iterator double_maptrace_map_iterator;

          double_maptrace_map_iterator = m_rtMapTraceMapSPtr->find(dt_key);

          // Tells if the key was already found in the map.
          if(double_maptrace_map_iterator != m_rtMapTraceMapSPtr->end())
            {
              // qDebug() << "Adding new key value:" << rt_or_dt_key;

              // The map already contained a pair that had the key value ==
              // dt_key. We want to sum, map item by map item all the map
              // items in the current_maptrace MapTrace to
              // the combined one that will hold the final color map data:
              // double_maptrace_map_iterator->second.

              // For example, thread 1 yielded a <double, MapTrace> map like
              // this: <rt, map<dt, tic>>. rt is dt_key and dt is rt_key.

              // We search in the member <double, MapTrace> map if an item has a
              // value matching rt (that is, dt_key). If found then we need
              // to combine the iterated MapTrace into the member MapTrace for
              // key dt_key. It's like if MapTrace were a mass spectrum, but
              // in fact m/z is replaced with dt.

              // qDebug() << "Before combination, map trace map size: "
              //<< double_map_trace_map_iterator->second.size();

              combiner.combine(double_maptrace_map_iterator->second,
                               current_maptrace);
            }
          else
            {

              // No map<double, MapTrace> item was found with double equals to
              // dt_key. So we need to create a MapTrace ex nihilo.

              pappso::MapTrace map_trace;

              combiner.combine(map_trace, current_maptrace);

              // Finally, store in the map, the map_trace along with its
              // dt_key.

              (*m_rtMapTraceMapSPtr)[dt_key] = map_trace;
            }
        }
    }

  // We now need to go through the double map trace map to inspect its
  // characteristics.

  if(!m_rtMapTraceMapSPtr->size())
    {
      qDebug() << "There are no data in the double maptrace map.";

      return;
    }

  m_colorMapMinKey = m_rtMapTraceMapSPtr->begin()->first;
  m_colorMapMaxKey = m_rtMapTraceMapSPtr->rbegin()->first;

  m_colorMapKeyCellCount = m_rtMapTraceMapSPtr->size();

  for(auto &&pair : *m_rtMapTraceMapSPtr)
    {
      pappso::MapTrace map_trace = pair.second;

      if(!map_trace.size())
        continue;

      if(map_trace.size() > m_colorMapMzCellCount)
        m_colorMapMzCellCount = map_trace.size();

      if(map_trace.begin()->first < m_colorMapMinMz)
        m_colorMapMinMz = map_trace.begin()->first;

      if(map_trace.rbegin()->first > m_colorMapMaxMz)
        m_colorMapMaxMz = map_trace.rbegin()->first;
    }

  std::chrono::system_clock::time_point chrono_end_time =
    std::chrono::system_clock::now();

  QString chrono_string;

  chrono_string = pappso::Utils::chronoIntervalDebugString(
    "Integration to RT / DT colormap took:",
    chrono_start_time,
    chrono_end_time);

  emit logTextToConsoleSignal(chrono_string);

  // qDebug().noquote() << chrono_string;

  return;
}


} // namespace minexpert

} // namespace msxps
