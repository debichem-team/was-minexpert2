libmass
=======

*libmass* is a C++ static library that is referenced from other projects.

The *libmass* static library is designed to enshrine the non-gui functionalities
needed by the following two projects:

* msXpertSuite/massXpert;
* msXpertSuite/mineXpert2.

*libmass* has a companion GUI library, *libmassgui* , that is designed to
enshrine the GUI functionalities needed by the same two projects above.

