/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright (C) 2009--2020 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once

#include <limits>
#include <vector>
#include <memory>

/////////////////////// Qt includes


/////////////////////// Local includes
#include "globals.hpp"
#include "IsotopicData.hpp"


namespace msxps
{
namespace libmass
{


class IsotopicDataBaseHandler
{

  public:
  IsotopicDataBaseHandler(const QString &file_name = QString());
  IsotopicDataBaseHandler(IsotopicDataSPtr isotopic_data_sp, const QString &file_name = QString());

  virtual ~IsotopicDataBaseHandler();

  // Load all the isotopic data from the file. file_name cannot be empty and
  // must point to a valid file, of course.
  virtual std::size_t loadData(const QString &file_name);

  // Write the isotopic data to a file. Here the is some leeway (file_name might
  // be empty, some alternatives will be searched for).
  virtual std::size_t writeData(const QString &file_name = QString());

  virtual void setIsotopicData(IsotopicDataSPtr isotopic_data_sp);
  virtual IsotopicDataSPtr getIsotopicData();

  virtual void setFileName(const QString &file_name);
  virtual QString getFileName();

  protected:
  IsotopicDataSPtr msp_isotopicData      = nullptr;
  QString m_fileName;

  virtual std::size_t checkConsistency();
};

typedef std::shared_ptr<IsotopicDataBaseHandler> IsotopicDataBaseHandlerSPtr;
typedef std::shared_ptr<const IsotopicDataBaseHandler>
  IsotopicDataBaseHandlerCstSPtr;

} // namespace libmass

} // namespace msxps

