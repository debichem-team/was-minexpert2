/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright (C) 2009--2020 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once

#include <limits>
#include <vector>
#include <memory>

/////////////////////// Qt includes


/////////////////////// pappsomspp includes
#include <pappsomspp/trace/datapoint.h>

/////////////////////// Local includes
#include "globals.hpp"
#include "Formula.hpp"
#include "IsotopicData.hpp"
#include "pappsomspp/trace/trace.h"
#include "pappsomspp/types.h"


namespace msxps
{
namespace libmass
{

enum class IsotopicDataType
{
  NOT_SET,
  LIBRARY_CONFIG,
  USER_CONFIG,
  MANUAL_CONFIG,
};


using FormulaChargePair         = std::pair<QString, int>;
using IsotopicClusterChargePair = std::pair<pappso::TraceCstSPtr, int>;


// We feed <formula, charge> pairs to the generator and it produces for each
// pair an isotopic cluster stored as a <pappso::TraceCstSPtr, charge> pair.
class IsotopicClusterGenerator
{

  public:
  IsotopicClusterGenerator();
  IsotopicClusterGenerator(libmass::IsotopicDataSPtr isotopic_data_sp);

  virtual ~IsotopicClusterGenerator();

  void setIsotopicDataType(IsotopicDataType isotopic_data_type);

  void setIsotopicData(libmass::IsotopicDataSPtr isotopic_data_sp);
  libmass::IsotopicDataSPtr getIsotopicData() const;

  void setFormulaChargePairs(
    const std::vector<FormulaChargePair> &formula_charge_pairs);

  void setFormulaChargePair(FormulaChargePair &formula_charge_pair);
  void appendFormulaChargePair(FormulaChargePair &formula_charge_pair);

  void setMaxSummedProbability(double max_probability);
  void setNormalizationIntensity(int normalize_intensity);
  void setSortType(pappso::SortType sort_type);
  void setSortOrder(pappso::SortOrder sort_order);

  bool configureIsotopicData(std::map<QString, int> &symbol_count_map,
                             int *&per_element_isotopes_count_array_p,
                             int *&per_element_symbol_count_array_p,
                             double **&per_element_isotope_masses_arrays_p_p,
                             double **&per_element_isotope_probs_arrays_p_p);

  bool validateFormula(Formula &formula);
  bool validateAllFormulas();

  pappso::TraceSPtr
  runIsotopicDataCalculations(std::size_t element_count,
                              int charge,
                              int *per_element_isotopes_count_array_p,
                              int *per_element_symbol_count_array_p,
                              double **per_element_isotope_masses_arrays_p_p,
                              double **per_element_isotope_probs_arrays_p_p);

  IsotopicClusterChargePair
  generateIsotopicClusterCentroids(FormulaChargePair formula_charge_pair);

  std::size_t run();

  QString clusterToString(const pappso::TraceCstSPtr &isotopic_cluster_sp) const;
  QString clustersToString() const;

  const std::vector<IsotopicClusterChargePair> &
  getIsotopicClusterChargePairs() const;

  protected:
  // Basic configuration
  libmass::IsotopicDataSPtr msp_isotopicData = nullptr;
  IsotopicDataType m_isotopicDataType        = IsotopicDataType::NOT_SET;

  double m_maxSummedProbability = 0.95;
  int m_normalizeIntensity      = std::numeric_limits<int>::min();

  // Convenience for the useur
  pappso::SortType m_sortType   = pappso::SortType::no_sort;
  pappso::SortOrder m_sortOrder = pappso::SortOrder::ascending;

  // Reminder
  // using FormulaChargePair         = std::pair<QString, int>;
  // using IsotopicClusterChargePair = std::pair<pappso::TraceCstSPtr, int>;

  // The starting material: a formula string and a corresponding charge.
  std::vector<FormulaChargePair> m_formulaChargePairs;

  // The clusters that are generated are stored using the charge.
  std::vector<IsotopicClusterChargePair> m_isotopicClusterChargePairs;

  void normalizeIntensities(pappso::TraceSPtr &isotopic_cluster_sp);
  void sortPeakCentroids(pappso::TraceSPtr &isotopic_cluster_sp);
  void sortPeakCentroidsByMz(pappso::TraceSPtr &isotopic_cluster_sp);
  void sortPeakCentroidsByIntensity(pappso::TraceSPtr &isotopic_cluster_sp);
};

typedef std::shared_ptr<IsotopicClusterGenerator> IsotopicClusterGeneratorSPtr;
typedef std::shared_ptr<const IsotopicClusterGenerator>
  IsotopicClusterGeneratorCstSPtr;

} // namespace libmass

} // namespace msxps

