/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright (C) 2009--2020 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once

/////////////////////// Std lib includes


/////////////////////// Qt includes
#include <QDebug>
#include <QCborStreamReader>
#include <QCborStreamWriter>


/////////////////////// PAPPSO includes
#include <pappsomspp/trace/datapoint.h>
#include <pappsomspp/trace/trace.h>

/////////////////////// Local includes
#include <libmass/globals.hpp>


namespace msxps
{

namespace libmass
{

enum class MassDataType
{
  UNSET = 0,
  MASS_SPECTRUM,
  DRIFT_SPECTRUM,
  TIC_CHROMATOGRAM,
  XIC_CHROMATOGRAM,
  MZ_RT_COLORMAP,
  DT_RT_COLORMAP,
  DT_MZ_COLORMAP,
};

typedef std::shared_ptr<QCborStreamReader> QCborStreamReaderSPtr;
typedef std::shared_ptr<QCborStreamWriter> QCborStreamWriterSPtr;

class MassDataCborBaseHandler : public QObject
{
  Q_OBJECT

  public:
  MassDataCborBaseHandler(QObject *parent_p);
  ~MassDataCborBaseHandler();

  void setInputFileName(const QString &file_name);
  void setOutputFileName(const QString &file_name);

  MassDataType getMassDataType() const;

  static MassDataType readMassDataType(const QString &input_file_name);
  static MassDataType readMassDataType(const QByteArray &byte_array);
  static MassDataType readMassDataType(QCborStreamReaderSPtr &reader_sp);

  void setMassDataType(MassDataType mass_data_type);

  virtual bool readFile(const QString &input_file_name = QString());
  virtual bool readByteArray(const QByteArray &byte_array);
  virtual bool writeFile(const QString &output_file_name = QString());
  virtual void writeByteArray(QByteArray &byte_array);

  void setTitle(const QString &title);
  QString getTitle() const;

  protected:
  MassDataType m_massDataType = MassDataType::UNSET;
  QString m_title             = QString();
  QString m_currentKey;

  QCborStreamReaderSPtr msp_reader = nullptr;
  QCborStreamWriterSPtr msp_writer = nullptr;

  QString m_inputFileName  = QString();
  QString m_outputFileName = QString();

  virtual bool readContext(QCborStreamReaderSPtr &reader_sp);
};


} // namespace libmass

} // namespace msxps

