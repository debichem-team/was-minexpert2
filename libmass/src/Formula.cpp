/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// Qt includes
#include <QChar>
#include <QString>


/////////////////////// Local includes
#include "Formula.hpp"


namespace msxps
{

namespace libmass
{


//! Construct a formula initialized with a formula string
/*!

  The formula is initialized using \p formula. Upon construction of
  the formula, no parsing occurs.

  \p formula gets simply copied into the \c m_formula.

  \param formula string representing a formula or an actionformula. Defaults
  to a null string.

*/
Formula::Formula(const QString &formula) : m_formula{formula}
{
}


Formula::Formula(const Formula &other)
  : m_formula{other.m_formula},
    m_plusFormula{other.m_plusFormula},
    m_minusFormula{other.m_minusFormula}
{
  m_symbolCountMap = other.m_symbolCountMap;
}


Formula::~Formula()
{
}


Formula &
Formula::operator=(const Formula &other)
{
  if(&other == this)
    return *this;

  m_formula        = other.m_formula;
  m_plusFormula    = other.m_plusFormula;
  m_minusFormula   = other.m_minusFormula;
  m_symbolCountMap = other.m_symbolCountMap;

  return *this;
}


//! Set the actionformula.
/*!

  The  \p formula is copied to \c this \c m_formula.  No other processing is
  performed.

  \param formula formula or actionformula initializer.
  */
void
Formula::setFormula(const QString &formula)
{
  m_formula = formula;
}


//! Set the formula or the actionformula.
/*!

  The \c m_formula member of the argument \p formula is copied to \c this \c
  m_formula.  No other processing is performed.

  \param formula \Formula from which to copy the \c m_formula.
  */
void
Formula::setFormula(const Formula &formula)
{
  m_formula = formula.m_formula;
}


void
Formula::appendFormula(const QString &formula)
{
  QString local_formula = formula.simplified();

  // Remove the spaces before appending.
  local_formula.remove(QRegularExpression("\\s+"));

  m_formula.append(local_formula);
}


//! Return the formula as a string.
/*!

  \return the formula as a string.

*/
QString
Formula::toString() const
{
  return m_formula;
}


void
Formula::setForceCountIndex(bool forceCountIndex)
{
  // When a formula contains a chemical element in a single copy, it is standard
  // practice to omit the count index: H2O is the same as H2O1. If
  // forceCountIndex is true, then the formula has to be in the form H2O1.
  // This is required for the IsoSpec-based calculations.
  m_forceCountIndex = forceCountIndex;
}


//! Clear all the formula member data.
/*!

  The AtomCount instances are freed also.

*/
void
Formula::clear()
{
  m_formula.clear();
  m_plusFormula.clear();
  m_minusFormula.clear();

  m_symbolCountMap.clear();
}


//! Set the \c m_plusFormula component.
/*!

  \param formula formula initializer.

*/
void
Formula::setPlusFormula(const QString &formula)
{
  m_plusFormula = formula;
}


//! Return the \c m_plusFormula component.
/*!

  \return the \c plusFormula component as a string.

*/
const QString &
Formula::plusFormula() const
{
  return m_plusFormula;
}


//! Set the \c m_minusFormula component.
/*!

  \param formula formula initializer.

*/
void
Formula::setMinusFormula(const QString &formula)
{
  m_minusFormula = formula;
}


//! Return the \c m_minusFormula component.
/*!

  \return the \c minusFormula component as a string.

*/
const QString &
Formula::minusFormula() const
{
  return m_minusFormula;
}


//! Test equality.
/*!

  The test only involves to the formula, not the minus-/plus-formulas nor the
  list of AtomCount instances.

  \param other formula to be compared with \c this.

  \return true if the formulas are identical, false otherwise.

*/
bool
Formula::operator==(const Formula &other) const
{
  return (m_formula == other.m_formula);
}


//! Test inequality.
/*!

  The test only involves to the formula, not the minus-/plus-formulas nor the
  list of AtomCount instances.

  \param other formula to be compared with \c this.

  \return true if the formulas differ, false otherwise.

*/
bool
Formula::operator!=(const Formula &other) const
{
  return (m_formula != other.m_formula);
}


//! Tell the actions found in the formula.
/*!

  Following analysis of the \p formula argument, this function will
  be able to tell if the formula contains only '+'-associated elements
  or also '-'-associated elements.

  If a formula contains no sign at all, then it is considered to contain only
  '+'-associated member(s). If no '+' sign is found and one member if found
  associated to a '-', then the minus action prevails. In other words, this
  function check if '-' members are found.

  This function is used to quickly have an indication if the
  splitActionParts() function is to be run or if it is not necessary.

  \param formula formula to report the actions about.

  \return '+' if no '-' action was found, '-' otherwise.

  \sa splitActionParts()
  */
QChar
Formula::actions(const QString &formula)
{
  int minusCount = formula.count('-', Qt::CaseInsensitive);

  return (minusCount == 0 ? '+' : '-');
}


//! Tell the actions found in \c this Formula instance.
/*!

  Following analysis of \c m_formula, this function will be able to tell if
  the formula contains only '+'-associated elements or also '-'-associated
  elements.

  If a formula contains no sign at all, then it is considered to contain only
  '+'-associated member(s). If no '+' sign is found and one member is found
  associated to a '-', then the minus action prevails. In other words, this
  function check if '-' members are found.

  This function is used to quickly have an indication if the
  splitActionParts() function is to be run or if it is not necessary.

  \param formula formula to report the actions about.

  \return '+' if no '-' action was found, '-' otherwise.

  \sa splitActionParts()
  */
QChar
Formula::actions() const
{
  return actions(m_formula);
}

//! Remove the title from the \c m_formula actionformula.
/*!

  The <em>title</em> of a formula is the documentation string, enclosed in
  double quotes, that is located in front of the actual chemical
  actionformula.  This function removes that <em>title</em> element from the
  \c m_formula actionformula. The \e title substring of the actionformula is
  removed using a QRegularExpression.

  \return the number of removed characters.

*/
int
Formula::removeTitle()
{
  int length = m_formula.length();

  m_formula.remove(QRegularExpression("\".*\""));

  return (length - m_formula.length());
}


//! Remove the space characters from the \c m_formula actionformula.
/*!

  Spaces can be placed anywhere in formula for more readability. However, it
  might be required that these character spaces be removed. This function does
  just this, using a QRegularExpression.

  \return the number of removed characters.

*/
int
Formula::removeSpaces()
{
  int length = m_formula.length();

  // We want to remove all the possibly-existing spaces.

  m_formula.remove(QRegularExpression("\\s+"));

  // Return the number of removed characters.
  return (length - m_formula.length());
}


//! Split the formula according to its plus-/minus- actions.
/*!

  Parses the \c m_formula actionformula and separates all the minus components
  of that actionformula from all the plus components. The different components
  are set to their corresponding formula (\c m_minusFormula and \c
  m_plusFormula).

  At the end of the split work, each sub-formula (plus- and/or minus-)
  is actually parsed for validity, using the reference atom list.

  If \p times is not 1, then the accounting of the plus/minus formulas is
  compounded by this factor.

  \param isotopic_data_csp chemical elements reference data.

  \param times Number of times the formula has to be accounted
  for. Defaults to 1.

  \param store Indicates if symbol/count information during the
  parsing of the sub-formulas generated by the split of the formula
  has to be stored, or not. Defaults to false.

  \param reset Indicates if the symbol/count information needs to be reset
  before the splitActionParts work. This parameter may be useful if the caller
  needs to "accumulate" the accounting of the formula (in which case it should
  be set to \c false). Defaults to false.

  \return FormulaSplitResult::FORMULA_SPLIT_FAIL if the splitting failed,
  FormulaSplitResult::FORMULA_SPLIT_PLUS if the components of the formula are
  all of type plus, FormulaSplitResult::FORMULA_SPLIT_MINUS if all the
  components of the formula are of type minus. The result value can be an
  OR'ing of FormulaSplitResult::FORMULA_SPLIT_PLUS and
  FormulaSplitResult::FORMULA_SPLIT_MINUS if both plus and minus components
  were found in the \c m_formula actionformula.

*/
int
Formula::splitActionParts(IsotopicDataCstSPtr isotopic_data_csp,
                          int times,
                          bool store,
                          bool reset)
{
  QChar curChar;
  QString tempFormula;

  int result = 0;

  bool wasParsingFormula = false;
  bool shouldBeFormula   = false;
  bool wasMinusSign      = false;

  if(!isotopic_data_csp->size())
    qFatal("Programming error. The isotopic data cannot be empty.");

  // We are asked to put all the '+' components of the formula
  // into corresponding formula and the same for the '-' components.

  m_plusFormula.clear();
  m_minusFormula.clear();

  // Because the formula that we are analyzing might contain a title
  // and spaces , we first remove these. But make a local copy of
  // the member datum.

  QString formula = m_formula;

  // qDebug() << "splitActionParts before working:"
  //<< "m_formula:" << m_formula;

  // One formula can be like this:

  // "Decomposed adenine" C5H4N5 +H

  // The "Decomposed adenine" is the title
  // The C5H4N5 +H is the formula.

  formula.remove(QRegularExpression("\".*\""));

  // We want to remove all the possibly-existing spaces.

  formula.remove(QRegularExpression("\\s+"));

  // If the formula does not contain any '-' character, then we
  // can approximate that all the formula is a '+' formula, that is a
  // plusFormula:

  if(actions() == '+')
    {
      // qDebug() << "Only plus actions.";

      m_plusFormula.append(formula);

      // At this point we want to make sure that we have a correct
      // formula. Remove all the occurrences of the '+' sign.
      m_plusFormula.replace(QString("+"), QString(""));

      if(m_plusFormula.length() > 0)
        {
          // qDebug() << "splitActionParts: with m_plusFormula:" <<
          // m_plusFormula;

          if(!parse(isotopic_data_csp, m_plusFormula, times, store, reset))
            return FORMULA_SPLIT_FAIL;
          else
            return FORMULA_SPLIT_PLUS;
        }
    }
  // End of
  // if(actions() == '+')

  // At this point, we truly have to iterate in the formula...

  for(int iter = 0; iter < formula.length(); ++iter)
    {
      curChar = formula.at(iter);
      // qDebug() << "curChar:" << curChar;

      if(curChar == '+' || curChar == '-')
        {
          if(shouldBeFormula)
            return FORMULA_SPLIT_FAIL;

          if(wasParsingFormula)
            {
              // We were parsing a formula, wich means that we are
              // ending that formula now, by starting another one. For
              // example, if we had "-CH3+COOH" we would typically be
              // at the '+' after having parsed -CH3. So we now have
              // to account for that latter formula.

              if(wasMinusSign)
                m_minusFormula.append(tempFormula);
              else
                m_plusFormula.append(tempFormula);

              // Reinit the tempFormula for next round.
              tempFormula.clear();

              // Now set proper bool values for next round.
              shouldBeFormula = true;
              wasMinusSign    = (curChar == '-' ? true : false);

              continue;
            }
          else
            {
              wasMinusSign    = (curChar == '-' ? true : false);
              shouldBeFormula = true;

              continue;
            }
        }
      else
        {
          // We are parsing either a digit or an alphabetical
          // character : we just append it to the tempFormula:
          tempFormula.append(curChar);

          wasParsingFormula = true;

          // We do not necessarily have to expect another formula
          // component at next round, admitting we were on the
          // nitrogen atom of CH3CN:
          shouldBeFormula = false;

          continue;
        }
    } // End for (int iter = 0 ; iter < formula.length() ; ++iter)

  // At this point the loop was finished so we might have something
  // interesting cooking:

  if(wasParsingFormula && tempFormula.length() > 0)
    {
      if(wasMinusSign)
        m_minusFormula.append(tempFormula);
      else
        m_plusFormula.append(tempFormula);
    }

  // At this point we want to make sure that we have a correct
  // formula. First reset the atomcount stuff if required.

  if(reset)
    m_symbolCountMap.clear();

  // qDebug() << "splitActionParts:"
  //<< "right after splitting:"
  //<< "m_formula:" << m_formula << "text" << toString();

  // Now that we have reset (if required) the atomCountList, we need not
  // and we must not reset during the parsing below, otherwise if we
  // have -H+H3PO4, then we'll compute +H3PO4 first, then we compute
  // -H with reset to true : the +H3PO4 component is destroyed!

  if(m_plusFormula.length() > 0)
    {
      bool res = parse(
        isotopic_data_csp, m_plusFormula, times, store, false /* reset */);

      // qDebug() << "splitActionParts:"
      //<< "right after parse of m_plusFormula:" << m_plusFormula
      //<< "m_formula:" << m_formula << "text" << toString();

      if(!res)
        return FORMULA_SPLIT_FAIL;
      else
        result = FORMULA_SPLIT_PLUS;
    }


  if(m_minusFormula.length() > 0)
    {
      bool res = parse(
        isotopic_data_csp, m_minusFormula, -times, store, false /* reset */);

      // qDebug() << "splitActionParts:"
      //<< "right after parse of m_minusFormula:" << m_minusFormula
      //<< "m_formula:" << m_formula << "text" << toString();

      if(!res)
        return FORMULA_SPLIT_FAIL;
      else
        result |= FORMULA_SPLIT_MINUS;
    }

  // qDebug() << "Formula" << formula << "splits"
  //<< "(+)" << m_plusFormula << "(-)" << m_minusFormula;

  return result;
}


//! Parse the \p formula using the reference atom list.
/*!

  Upon parsing of the formula, a list of AtomCount objects are created in
  order to be able to account for the mass of the formula.

  If \p times is greater than one, then the mass contribution of the formula
  is compounded by this factor.

  \param refList List of reference atoms.

  \param formula Formula to parse.

  \param times Number of times that the formula should be accounted
  for. Default value is 1.

  \param store Indicates if AtomCount objects created during the
  parsing of the formula have to be stored, or not. Default value is
  false.

  \param reset Indicates if AtomCount objects created during a previous
  parsing of the formula have to be destroyed before doing another parsing.
  This parameter is interesting if the caller needs to "accumulate" the
  accounting of the formula. Default value is false.

  \return true if parsing succeeded, false otherwise.

*/
bool
Formula::parse(IsotopicDataCstSPtr isotopic_data_csp,
               const QString &formula,
               int times,
               bool store,
               bool reset)
{
  QChar curChar;
  QString parsedCount;
  QString parsedSymbol;

  bool wasDigit = false;
  bool gotUpper = false;

  QString element_symbol = "";
  int element_count      = 0;

  if(!isotopic_data_csp->size())
    qFatal("Programming error. The isotopic data cannot be empty.");

  // The formula member is a QString that should hold the formula
  // according to this typical schema: "H2O" (for water). That means we
  // only want letters(Upper and lower case and number).

  // If reset is true, then clear the symbol count member map. Otherwise, the
  // findings during this parse are aggregated to that map.

  // The formula should thus not be empty, otherwise there is nothing
  // to do. But it is not an error that the formula be empty.
  if(formula.length() == 0)
    return true;

  if(!checkSyntax(formula))
    {
      qDebug() << "Formula check failed.";
      return false;
    }

  // qDebug() << "The formula checked fine.";

  // Also, the first character of the formula should be an Uppercase
  // letter. If not, logically, the formula is incorrect.
  if(formula.at(0).category() != QChar::Letter_Uppercase)
    return false;

  if(reset)
    m_symbolCountMap.clear();

  // And now finally start the real parsing stuff.

  for(int iter = 0; iter < formula.length(); ++iter)
    {
      curChar = formula.at(iter);

      // qDebug() << "Current char:" << curChar;

      if(curChar.category() == QChar::Number_DecimalDigit)
        {
          // We are parsing a digit.

          parsedCount.append(curChar);

          wasDigit = true;

          continue;
        }
      else if(curChar.category() == QChar::Letter_Lowercase)
        {
          // Current character is lowercase, which means we are inside
          // of an atom symbol, such as Ca(the 'a') or Nob(either
          // 'o' or 'b'). Thus, gotUpper should be true !

          if(!gotUpper)
            return false;

          // Make use of the parsed numerical character.
          parsedSymbol.append(curChar);

          // Let the people know that we have parsed a lowercase char
          // and not a digit.
          wasDigit = false;
        }
      else if(curChar.category() == QChar::Letter_Uppercase)
        {
          // Current character is uppercase, which means that we are
          // at the beginning of an atom symbol. Check if there was a
          // symbol being parsed before this one.

          if(parsedSymbol.isEmpty())
            {
              // Start new parsing round.
              parsedSymbol.append(curChar);

              gotUpper = true;
              wasDigit = false;
              continue;
            }

          // There was a symbol being parsed. Fill-in the atomCount
          // object.
          element_symbol = parsedSymbol;

          // Now we can prepare the field for the next one.
          parsedSymbol.clear();
          parsedSymbol.append(curChar);

          // Before going on, check if the symbol is correct.
          // qDebug() << "Now checking if isotopic data contain symbol"
          //<< element_symbol;

          if(!isotopic_data_csp->containsSymbol(element_symbol))
            {
              qDebug() << "The isotopic data do not contain symbol"
                       << element_symbol;
              return false;
            }

          // qDebug() << "The isotopic data do contain symbol" <<
          // element_symbol;

          // If there was a count being parsed, we have to take it
          // into account.
          if(wasDigit)
            {
              bool ok       = false;
              element_count = parsedCount.toInt(&ok);

              if(!ok)
                // The atom counts for nothing ! Or was there
                // an error in the conversion ?
                return false;

              // But we remember that we have to take into account the
              // times parameter.
              element_count *= times;

              // Clear parsedCount for next count parsing round.
              parsedCount.clear();
            }
          else
            element_count = times;

          // Account the new symbol/count pair in the symbol/count member map.

          if(store)
            {
              // qDebug() << "Accounting symbol pair:" << element_symbol << "/"
              //<< element_count;

              accountSymbolCountPair(element_symbol, element_count);

              // qDebug() << " ...done.";
            }

          // Let the people know what we got:

          wasDigit = false;
          gotUpper = true;
        }
      // end(curChar.category() == QChar::Letter_Uppercase)
    }
  // end for (int iter = 0 ; iter < formula.length() ; ++iter)

  // At this point we are at then end of the string, and we thus might
  // still have something cooking:

  // Thus we have to check that the last parsed atom
  // symbol is correct.

  element_symbol = parsedSymbol;

  if(!isotopic_data_csp->containsSymbol(element_symbol))
    {
      qDebug() << "Symbol" << element_symbol
               << "not found in the isotopic data.";
      return false;
    }

  // qDebug() << "Symbol" << element_symbol << "was found in the isotopic
  // data.";

  // And now we have to convert the string representation
  // of the atom count for that atom to int. In fact, we
  // have to be able to know that water H2O has TWO
  // hydrogen atoms in it.

  // If there was a count being parsed, we have to take it
  // into account.
  if(wasDigit)
    {
      bool ok       = true;
      element_count = parsedCount.toInt(&ok);

      if(!ok)
        // The atom counts for nothing ! Or was there
        // an error in the conversion ?
        return false;

      // But we remember that we have to take into account the
      // times parameter.
      element_count *= times;
    }
  else
    element_count = times;

  // Finally, if asked by the caller, we can account for
  // this atom symbol/count also !

  if(store)
    {
      // qDebug() << "Accounting symbol pair:" << element_symbol << "/"
      //<< element_count;

      accountSymbolCountPair(element_symbol, element_count);

      // qDebug() << " ...done.";
    }

  return true;
}


// We want an integer because times might be < 0.
int
Formula::accountSymbolCountPair(const QString &symbol, int times)
{
  // We receive a symbol and we need to account for it times times in the member
  // symbol count map (times might be < 0).

  // Try to insert the new symbol/count pairinto the map. Check if that was done
  // or not. If the result.second is false, then that means the the insert
  // function did not perform because a pair by that symbol existed already. In
  // that case we just need to increment the count for the the pair.

  // qDebug() << "Accounting symbol:" << symbol << "for count:" << times;

  int new_count = 0;

  std::pair<std::map<QString, int>::iterator, bool> res =
    m_symbolCountMap.insert(std::pair<QString, std::size_t>(symbol, times));

  if(!res.second)
    {
      // qDebug() << "The symbol was already in the symbol/count map.";

      // One pair by that symbol key existed already, just update the count and
      // store that value for reporting.
      res.first->second += times;
      new_count = res.first->second;
      // new_count might be <= 0.

      // qDebug() << "For symbol" << symbol << "the new count:" << new_count;
    }
  else
    {
      // qDebug() << "Symbol" << symbol
      //<< "was not there already, setting the count to:" << times;

      // We just effectively add a new pair to the map by key symbol.
      new_count = times;
    }

  // We should check if the symbol has now a count of 0. In that case, we remove
  // the symbol altogether because we do not want to list naught symbols in the
  // final formula.

  if(!new_count)
    {
      // qDebug() << "For symbol" << symbol
      //<< "the new count is 0. Thus we erase the map item altogether.";

      m_symbolCountMap.erase(symbol);
    }

  // Update what's the text of the formula to represent what is in
  // atomCount list.

  // qDebug() << "Now computing the new formula as an elemental composition.";
  m_formula = elementalComposition();

  // qDebug() << "... Done: " << m_formula;

  return new_count;
}


const std::map<QString, int>
Formula::getSymbolCountMap() const
{
  return m_symbolCountMap;
}


std::size_t
Formula::accountFormula(const QString &text,
                        IsotopicDataCstSPtr isotopic_data_csp,
                        int times)
{
  //qDebug() << "Accounting in this formula:" << m_formula
           //<< "the external formula:" << text;

  //qDebug() << "Before having merged the external formula's map into this one, "
              //"this one has size:"
           //<< m_symbolCountMap.size();

  // We get a formula as an elemental composition text string and we want to
  // account for that formula in *this formula.

  // First off, validate the text.

  Formula formula(text);

  // The formula is asked to validate with storage of the found symbol/count
  // pairs and with resetting of the previous contents of the symbol/count map.
  if(!formula.validate(isotopic_data_csp, true, true))
    {
      qDebug() << "Formula:" << text << "failed to validate.";
      return -1;
    }

  // Now, for each item in the formula's symbol/count map, aggregate the found
  // data to *this symbol/count map. We'll have "merged" or "aggreated" the
  // other formula into *this one.

  std::map<QString, int> map_copy = formula.getSymbolCountMap();

  // qDebug()
  //<< "The external formula, after validation has a symbol/count map of size:"
  //<< map_copy.size();

  std::map<QString, int>::const_iterator iter     = map_copy.cbegin();
  std::map<QString, int>::const_iterator iter_end = map_copy.cend();

  while(iter != iter_end)
    {
      accountSymbolCountPair(iter->first, iter->second * times);
      ++iter;
    }

  //qDebug() << "After having merged the external formula's map into this one, "
              //"this one has size:"
           //<< m_symbolCountMap.size();

  // Update what's the text of the formula to represent what is in
  // atomCount list.
  m_formula = elementalComposition();

  //qDebug() << "And now this formula has text: " << m_formula;

  return m_symbolCountMap.size();
}


//! Check the syntax of the \p formula.
/*!

  The syntax of the \p formula is checked by verifying that the
  letters and ciphers in the formula are correctly placed. That is, we
  want that the ciphers appear after an atom symbol and not before
  it. We want that the atom symbol be made of one uppercase letter and
  that the following letters be lowercase.

  \attention This is a syntax check and not a true validation, as the formula
  can contain symbols that are syntactically valid but corresponding to atom
  definitions not available on the system. Indeed, there is no comparison of
  the symbols with those in an Atom reference list in this function. See the
  validate() function for a more in-depth check of the formula.

  \param formula the formula to check.

  \return true upon successful check, false otherwise.

  \sa validate().

*/
bool
Formula::checkSyntax(const QString &formula, bool forceCountIndex)
{
  // Static function.

  // qDebug() << "Checking syntax with formula:" << formula;

  QChar curChar;

  bool gotUpper = false;
  bool wasSign  = false;
  bool wasDigit = false;

  // Because the formula that we are analyzing might contain a title
  // and spaces , we first remove these. But make a local copy of
  // the member datum.

  QString localFormula = formula;

  // One formula can be like this:

  // "Decomposed adenine" C5H4N5 +H

  // The "Decomposed adenine" is the title
  // The C5H4N5 +H is the formula.

  localFormula.remove(QRegularExpression("\".*\""));

  // We want to remove all the possibly-existing spaces.

  localFormula.remove(QRegularExpression("\\s+"));


  for(int iter = 0; iter < localFormula.length(); ++iter)
    {
      curChar = localFormula.at(iter);

      // qDebug() << __FILE__ << "@" << __LINE__ << __FUNCTION__ << "()"
      //<< "Current character:" << curChar;

      // FIXME One improvement that would ease modelling the Averagine would
      // be to silently allow double formula indices (that is, double atom
      // counts). They would not be compulsory

      if(curChar.category() == QChar::Number_DecimalDigit)
        {
          // We are parsing a digit.

          // We may not have a digit after a +/- sign.
          if(wasSign)
            return false;

          wasSign  = false;
          wasDigit = true;

          continue;
        }
      else if(curChar.category() == QChar::Letter_Lowercase)
        {
          // Current character is lowercase, which means we are inside
          // of an atom symbol, such as Ca(the 'a') or Nob(either
          // 'o' or 'b'). Thus, gotUpper should be true !

          if(!gotUpper)
            return false;

          // We may not have a lowercase character after a +/- sign.
          if(wasSign)
            return false;

          // Let the people know that we have parsed a lowercase char
          // and not a digit.
          wasSign = false;

          wasDigit = false;
        }
      else if(curChar.category() == QChar::Letter_Uppercase)
        {
          // Current character is uppercase, which means that we are
          // at the beginning of an atom symbol.

          // There are two cases:
          // 1. We are starting for the very beginning of the formula, and
          // nothing came before this upper case character. That's fine.
          // 2. We had previously parsed a segment of the formula, and in this
          // case, we are closing a segment. If the parameter
          // obligatoryCountIndex is true, then we need to ensure that the
          // previous element had an associated number, even it the count
          // element is 1. This is required for the IsoSpec stuff in the gui
          // programs.

          if(iter > 0)
            {
              if(forceCountIndex)
                {
                  if(!wasDigit)
                    {
                      qDebug()
                        << "Returning false because upper case char was not"
                           "preceded by digit while not at the first char of "
                           "the formula";

                      return false;
                    }
                }
            }

          // Let the people know what we got:

          wasSign  = false;
          gotUpper = true;
          wasDigit = false;
        }
      else
        {
          if(curChar != '+' && curChar != '-')
            return false;
          else
            {
              // We may not have 2 +/- signs in a raw.
              if(wasSign)
                return false;
            }

          wasSign  = true;
          gotUpper = false;
          wasDigit = false;
        }
    }
  // end for (int iter = 0 ; iter < localFormula.length() ; ++iter)

  // Note that if we want an obligatory count index, then, at the end of the
  // formula, *compulsorily* we must have parsed a digit.

  if(forceCountIndex && !wasDigit)
    {
      qDebug()
        << "Returning false because the formula does not end with a digit.";

      return false;
    }

  // At this point we found no error condition.
  return true;
}


//! Check the syntax of \c this Formula \c m_formula actionformula member.
/*!

  The syntax of the \c m_formula is checked by verifying that the letters and
  ciphers in the formula are correctly placed. That is, we want that the
  ciphers appear after an atom symbol and not before it. We want that the atom
  symbol be made of one uppercase letter and that the following letters be
  lowercase.

  Note that the checking only concerns \c m_formula, and not the
  minus-/plus- formulas.

  \attention This is a syntax check and not a true validation, as the formula
  can contain symbols that are syntactically valid but corresponding to atom
  definitions not available on the system. Indeed, there is no comparison of
  the symbols with those in an Atom reference list in this function. See the
  validate() function for a more in-depth check of the formula.

  \return true upon successful check, false otherwise.

  \sa validate().

*/
bool
Formula::checkSyntax() const
{
  // The default formula is always m_formula.

  return checkSyntax(m_formula, m_forceCountIndex);
}


//! Validate the formula.
/*! The validation of the formula involves:

  - Checking that the formula is not empty;

  - Splitting that formula into its plus-/minus- parts and parse the obtained
  plus-/minus- formulas. During parsing of the minus-/plus- formulas, each
  atom symbol encountered in the formulas is validated against the \p refList
  reference atom list;

  - Checking that at least the plus- or the minus- part contains
  something (same idea that the formula cannot be empty).

  \param refList List of reference atoms.

  \param store Indicates if AtomCount objects created during the
  parsing of the sub-formulas generated by the split of the formula
  have to be stored, or not. Defaults to false.

  \param reset Indicates if the list of AtomCount objects has to be reset
  before the splitActionParts work. This parameter may be useful in case the
  caller needs to "accumulate" multiple times an accounting of the formula.
  Defaults to false.

  \return true if the validation succeeded, false otherwise.
  */
bool
Formula::validate(IsotopicDataCstSPtr isotopic_data_csp, bool store, bool reset)
{
  if(isotopic_data_csp == nullptr || isotopic_data_csp.get() == nullptr)
    qFatal("Programming error. The isotopic data pointer cannot be nullptr.");

  if(!isotopic_data_csp->size())
    qFatal("Programming error. The isotopic data cannot be empty.");

  if(!m_formula.size())
    {
      qDebug() << "The formula is empty.";
      return false;
    }

  // qDebug() << "Now splitting the formula into its action parts.";

  int result = splitActionParts(isotopic_data_csp, 1, store, reset);

  if(result == FORMULA_SPLIT_FAIL)
    {
      qDebug() << "Failed splitting the formula into its action parts.";
      return false;
    }

  // Both the action formulas cannot be empty.
  if(!m_plusFormula.size() && !m_minusFormula.size())
    {
      qDebug() << "Both the plus and minus formulas are empty.";
      return false;
    }

  // qDebug() << "Success: the formula validated fine.";

  return true;
}


//! Account \p this formula's mono/avg masses.
/*!

  The masses corresponding to the \c m_formula member are calculated first and
  then the \p mono and \p avg parameters are updated using the calculated
  values. The formula accounting to the masses can be compounded by the \p
  times factor.

  \param refList List of atoms to be used as reference.

  \param mono Pointer to the monoisotopic mass to be updated. Defaults
  to Q_NULLPTR, in which case the value is not updated.

  \param avg Pointer to the average mass to be updated. Defaults to Q_NULLPTR,
  in which case the value is not updated.

  \param times Factor by which the accounting of the \c m_formula to the
  masses should be compounded.

  \return true upon success, false otherwise, for example if the
  splitActionParts() call fails.

  \sa splitActionParts().

*/
bool
Formula::accountMasses(IsotopicDataCstSPtr isotopic_data_csp,
                       double *mono,
                       double *avg,
                       int times)
{
  // Note the 'times' param below that ensures we create proper symbol/count
  // map items by taking that compounding factor into account.

  if(isotopic_data_csp == nullptr)
    qFatal("Programming error. The pointer cannot be nullptr.");

  if(splitActionParts(
       isotopic_data_csp, times, true /* store */, true /* reset */) ==
     FORMULA_SPLIT_FAIL)
    return false;

  // qDebug() << "Formula::accountMasses:"
  //<< "after splitActionParts:"
  //<< "store: true ; reset: true"
  //<< "m_formula:" << m_formula << "text" << Formula::toString();

  // At this point m_symbolCountMap has all the symbol/count pairs needed to
  // account for the masses.

  std::map<QString, int>::const_iterator iter     = m_symbolCountMap.cbegin();
  std::map<QString, int>::const_iterator iter_end = m_symbolCountMap.cend();

  // for(auto item : m_symbolCountMap)
  // qDebug() << "One symbol count item:" << item.first << "/" << item.second;

  bool ok = false;

  while(iter != iter_end)
    {
      QString symbol = iter->first;

      // qDebug() << "Getting masses for symbol:" << symbol;

      if(mono != nullptr)
        {
          double mono_mass =
            isotopic_data_csp->getMonoMassBySymbol(iter->first, &ok);

          if(!ok)
            {
              qDebug() << "Failed to get the mono mass.";
              return false;
            }

          *mono += mono_mass * iter->second;
        }

      ok = false;

      if(avg != nullptr)
        {
          double avg_mass =
            isotopic_data_csp->getAvgMassBySymbol(iter->first, &ok);

          if(!ok)
            return false;

          *avg += avg_mass * iter->second;
        }

      ++iter;
    }

  return true;
}


//! Account \p this \c m_formula to the \p Ponderable.
/*!

  The masses corresponding to the \c m_formula member are calculated first and
  then \p ponderable is updated using the calculated values. The formula
  accounting to \p ponderable can be compounded by the \p times factor.

  \param refList List of atoms to be used as reference.

  \param ponderable Pointer to the ponderable to be updated. Cannot be
  Q_NULLPTR.

  \param times Factor by which the accounting of the \c m_formula to \p
  ponderable should be compounded.

  \return true upon success, false otherwise, for example if the call to
  splitPart() fails.

  \sa splitActionParts().

*/
bool
Formula::accountMasses(IsotopicDataCstSPtr isotopic_data_csp,
                       Ponderable *ponderable,
                       int times)
{
  if(ponderable == nullptr)
    qFatal("Fatal error: pointer cannot be nullptr. Program aborted.");

  return accountMasses(
    isotopic_data_csp, &ponderable->rmono(), &ponderable->ravg(), times);


  //// Note the 'times' param below.
  // if(splitActionParts(isotopic_data_csp, times, true, true) ==
  // FORMULA_SPLIT_FAIL)
  // return false;

  //// At this point m_symbolCountMap has all the symbol/count pairs needed to
  //// account for the masses.

  // std::map<QString, int>::const_iterator iter     =
  // m_symbolCountMap.cbegin(); std::map<QString, int>::const_iterator iter_end
  // = m_symbolCountMap.cend();

  // while(iter != iter_end)
  //{
  // double mono_mass = isotopic_data_csp->getMonoMassBySymbol(iter->first);
  // ponderable->rmono() += mono_mass * iter->second;

  // double avg_mass = isotopic_data_csp->getAvgMassBySymbol(iter->first);
  // ponderable->ravg() += avg_mass * iter->second;

  //++iter;
  //}

  return true;
}


//! Account the atoms in \c this \c m_formula.
/*!

  Calls splitActionParts() to actually parse \c m_formula and account its atoms
  in \c m_atomCountList.

  \param refList List of atoms to be used as reference.

  \param times Factor by which the accounting of \c m_formula should be
  compounded.

  \return true upon success, false otherwise, if the call to splitActionParts()
  fails.

  \sa splitActionParts().
  */
bool
Formula::accountSymbolCounts(IsotopicDataCstSPtr isotopic_data_csp, int times)
{
  // Note the 'times' param below.
  if(splitActionParts(isotopic_data_csp, times, true, false) ==
     FORMULA_SPLIT_FAIL)
    return false;

  return true;
}


QString
Formula::elementalComposition(
  std::vector<std::pair<QString, int>> *dataVector) const
{
  // Iterate in the symbol count member map and for each item output the symbol
  // string accompanied by the corresponding count. Note that the count for any
  // given symbol might be negative. We want to craft an elemental composition
  // that accounts for "actions", that is a +elemental formula and a -elemental
  // formula.

  std::map<QString, int>::const_iterator iter     = m_symbolCountMap.cbegin();
  std::map<QString, int>::const_iterator iter_end = m_symbolCountMap.cend();

#if 0

  qDebug() << "While computing the elemental composition corresponding to the "
              "symbol/count map:";
  for(auto pair : m_symbolCountMap)
    qDebug().noquote() << "(" << pair.first << "," << pair.second << ")";

#endif

  QStringList negativeStringList;
  QStringList positiveStringList;

  while(iter != iter_end)
    {
      QString symbol = iter->first;
      int count      = iter->second;

      if(count < 0)
        {
          negativeStringList.append(
            QString("%1%2").arg(symbol).arg(-1 * count));
        }
      else
        {
          positiveStringList.append(QString("%1%2").arg(symbol).arg(count));
        }

      ++iter;
    }

  // We want to provide a formula that lists the positive component
  // first and the negative component last.

  // Each positive/negative component will list the atoms in the
  // conventional order : CxxHxxNxxOxx and all the rest in
  // alphabetical order.

  // We want to provide for each positive and negative components of the
  // initial formula object, an elemental formula that complies with the
  // convention : first the C atom, next the H, N, O, S, P atoms and all the
  // subsequent ones in alphabetical order.

  // Sort the lists.
  negativeStringList.sort();
  positiveStringList.sort();

  // Thus we look for the four C, H, N, O, S,P atoms, and we create the
  // initial part of the elemental formula. Each time we find one
  // such atom we remove it from the list, so that we can later just
  // append all the remaining atoms, since we have sorted the lists
  // above.

  // The positive component
  // ======================

  int atomIndex = 0;
  QString positiveComponentString;

  // Carbon
  atomIndex = positiveStringList.indexOf(QRegularExpression("C\\d+"));
  if(atomIndex != -1)
    {
      positiveComponentString += positiveStringList.at(atomIndex);
      positiveStringList.removeAt(atomIndex);

      if(dataVector)
        dataVector->push_back(
          std::pair<QString, int>("C", m_symbolCountMap.at("C")));
    }

  // Hydrogen
  atomIndex = positiveStringList.indexOf(QRegularExpression("H\\d+"));
  if(atomIndex != -1)
    {
      positiveComponentString += positiveStringList.at(atomIndex);
      positiveStringList.removeAt(atomIndex);

      if(dataVector)
        dataVector->push_back(
          std::pair<QString, int>("H", m_symbolCountMap.at("H")));
    }

  // Nitrogen
  atomIndex = positiveStringList.indexOf(QRegularExpression("N\\d+"));
  if(atomIndex != -1)
    {
      positiveComponentString += positiveStringList.at(atomIndex);
      positiveStringList.removeAt(atomIndex);

      if(dataVector)
        dataVector->push_back(
          std::pair<QString, int>("N", m_symbolCountMap.at("N")));
    }

  // Oxygen
  atomIndex = positiveStringList.indexOf(QRegularExpression("O\\d+"));
  if(atomIndex != -1)
    {
      positiveComponentString += positiveStringList.at(atomIndex);
      positiveStringList.removeAt(atomIndex);

      if(dataVector)
        dataVector->push_back(
          std::pair<QString, int>("O", m_symbolCountMap.at("O")));
    }

  // Sulfur
  atomIndex = positiveStringList.indexOf(QRegularExpression("S\\d+"));
  if(atomIndex != -1)
    {
      positiveComponentString += positiveStringList.at(atomIndex);
      positiveStringList.removeAt(atomIndex);

      if(dataVector)
        dataVector->push_back(
          std::pair<QString, int>("S", m_symbolCountMap.at("S")));
    }

  // Phosphorus
  atomIndex = positiveStringList.indexOf(QRegularExpression("P\\d+"));
  if(atomIndex != -1)
    {
      positiveComponentString += positiveStringList.at(atomIndex);
      positiveStringList.removeAt(atomIndex);

      if(dataVector)
        dataVector->push_back(
          std::pair<QString, int>("P", m_symbolCountMap.at("P")));
    }

  // Go on with all the other ones, if any...

  for(int iter = 0; iter < positiveStringList.size(); ++iter)
    {
      positiveComponentString += positiveStringList.at(iter);

      QRegularExpression regexp("([A-Z][a-z]*)(\\d+)");
      QRegularExpressionMatch match = regexp.match(positiveStringList.at(iter));

      if(match.hasMatch())
        {
          QString symbol  = match.captured(1);
          QString howMany = match.captured(2);

          bool ok   = false;
          int count = howMany.toInt(&ok, 10);

          if(!count && !ok)
            qFatal(
              "Fatal error at %s@%d -- %s(). "
              "Failed to parse an atom count."
              "Program aborted.",
              __FILE__,
              __LINE__,
              __FUNCTION__);

          if(dataVector)
            dataVector->push_back(std::pair<QString, int>(symbol, count));
        }
    }

  // qDebug() << __FILE__ << __LINE__
  //<< "positiveComponentString:" << positiveComponentString;


  // The negative component
  // ======================

  atomIndex = 0;
  QString negativeComponentString;

  // Carbon
  atomIndex = negativeStringList.indexOf(QRegularExpression("C\\d+"));
  if(atomIndex != -1)
    {
      negativeComponentString += negativeStringList.at(atomIndex);
      negativeStringList.removeAt(atomIndex);

      if(dataVector)
        dataVector->push_back(
          std::pair<QString, int>("C", m_symbolCountMap.at("C")));
    }

  // Hydrogen
  atomIndex = negativeStringList.indexOf(QRegularExpression("H\\d+"));
  if(atomIndex != -1)
    {
      negativeComponentString += negativeStringList.at(atomIndex);
      negativeStringList.removeAt(atomIndex);

      if(dataVector)
        dataVector->push_back(
          std::pair<QString, int>("H", m_symbolCountMap.at("H")));
    }

  // Nitrogen
  atomIndex = negativeStringList.indexOf(QRegularExpression("N\\d+"));
  if(atomIndex != -1)
    {
      negativeComponentString += negativeStringList.at(atomIndex);
      negativeStringList.removeAt(atomIndex);

      if(dataVector)
        dataVector->push_back(
          std::pair<QString, int>("N", m_symbolCountMap.at("N")));
    }

  // Oxygen
  atomIndex = negativeStringList.indexOf(QRegularExpression("O\\d+"));
  if(atomIndex != -1)
    {
      negativeComponentString += negativeStringList.at(atomIndex);
      negativeStringList.removeAt(atomIndex);

      if(dataVector)
        dataVector->push_back(
          std::pair<QString, int>("O", m_symbolCountMap.at("O")));
    }

  // Sulfur
  atomIndex = negativeStringList.indexOf(QRegularExpression("S\\d+"));
  if(atomIndex != -1)
    {
      negativeComponentString += negativeStringList.at(atomIndex);
      negativeStringList.removeAt(atomIndex);

      if(dataVector)
        dataVector->push_back(
          std::pair<QString, int>("S", m_symbolCountMap.at("S")));
    }

  // Phosphorus
  atomIndex = negativeStringList.indexOf(QRegularExpression("P\\d+"));
  if(atomIndex != -1)
    {
      negativeComponentString += negativeStringList.at(atomIndex);
      negativeStringList.removeAt(atomIndex);

      if(dataVector)
        dataVector->push_back(
          std::pair<QString, int>("P", m_symbolCountMap.at("P")));
    }

  // Go on with all the other ones, if any...

  for(int iter = 0; iter < negativeStringList.size(); ++iter)
    {
      negativeComponentString += negativeStringList.at(iter);

      QRegularExpression regexp("([A-Z][a-z]*)(\\d+)");
      QRegularExpressionMatch match = regexp.match(negativeStringList.at(iter));

      if(match.hasMatch())
        {
          QString symbol  = match.captured(1);
          QString howMany = match.captured(2);

          bool ok   = false;
          int count = howMany.toInt(&ok, 10);

          if(!count && !ok)
            qFatal(
              "Fatal error at %s@%d -- %s(). "
              "Failed to parse an atom count."
              "Program aborted.",
              __FILE__,
              __LINE__,
              __FUNCTION__);

          if(dataVector)
            dataVector->push_back(std::pair<QString, int>(symbol, count));
        }
    }


  // qDebug() << __FILE__ << __LINE__
  //<< "negativeComponentString:" << negativeComponentString;

  // Create the final elemental formula that comprises both the
  // positive and negative element. First the positive element and
  // then the negative one. Only append the negative one, prepended
  // with '-' if the string is non-empty.

  QString elementalComposition = positiveComponentString;

  if(!negativeComponentString.isEmpty())
    elementalComposition += QString("-%1").arg(negativeComponentString);

  // qDebug() << __FILE__ << __LINE__
  // <<"elementalComposition:" << elementalComposition;

  return elementalComposition;
}


//! Compute the total number of atoms.
/*!

  Iterates in the \m m_atomCountList and for each AtomCount instance
  increments a total atom count by the count of that AtomCount instance.

  \return The number of atoms.

*/
int
Formula::totalAtoms() const
{

  int total_atom_count = 0;

  std::map<QString, int>::const_iterator iter     = m_symbolCountMap.cbegin();
  std::map<QString, int>::const_iterator iter_end = m_symbolCountMap.cend();

  while(iter != iter_end)
    total_atom_count += iter->second;

  return total_atom_count;
}


//! Compute the total number of isotopes.
/*!

  Iterate in the \m m_atomCountList and for each AtomCount instance finds the
  corresponding Atom in \p refList. The Atom is then used to get the number of
  Isotope instances it is made of. These are accounted for in a total count.

  \param refList List of atoms to be used as reference.

  \return The total count of isotopes in \c this \c m_atomCountList member.
  */
int
Formula::totalIsotopes(IsotopicDataCstSPtr isotopic_data_csp) const
{
  int total_isotope_count = 0;

  std::map<QString, int>::const_iterator iter     = m_symbolCountMap.cbegin();
  std::map<QString, int>::const_iterator iter_end = m_symbolCountMap.cend();

  while(iter != iter_end)
    total_isotope_count +=
      iter->second * isotopic_data_csp->getIsotopeCountBySymbol(iter->first);

  return total_isotope_count;
}


int
Formula::symbolCount(const QString &symbol) const
{
  // Return the symbol index.

  std::map<QString, int>::const_iterator iter_end = m_symbolCountMap.cend();

  std::map<QString, int>::const_iterator iter = m_symbolCountMap.find(symbol);

  if(iter == iter_end)
    return 0;

  return iter->second;
}


bool
Formula::hasNetMinusPart()
{
  return m_minusFormula.size();
}


//! Parse a formula XML element and set the data to \c m_formula.
/*!

  Parses the formula XML element passed as argument and sets the
  data of that element to \p this formula instance(this is called XML
  rendering). The syntax of the parsed formula is checked and the
  result of that check is returned.

  \param element XML element to be parsed and rendered.

  \return true if parsing and syntax checking were successful, false
  otherwise.

  */
bool
Formula::renderXmlFormulaElement(const QDomElement &element)
{
  if(element.tagName() != "formula")
    return false;

  m_formula = element.text();

  // qDebug() << "Rendering formula element with text:" << m_formula;

  // Do not forget that we might have a title associated with the
  // formula and spaces. checkSyntax() should care of removing these
  // title and spaces before checking for chemical syntax
  // correctness.

  return checkSyntax();
}

} // namespace libmass

} // namespace msxps
