/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright (C) 2009--2020 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// Std lib includes

/////////////////////// Qt includes
#include <QDebug>


/////////////////////// IsoSpec
#include <IsoSpec++/isoSpec++.h>
#include <IsoSpec++/element_tables.h>


// extern const int elem_table_atomicNo[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];
// extern const double
// elem_table_probability[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];
// extern const double elem_table_mass[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];
// extern const int elem_table_massNo[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];
// extern const int
// elem_table_extraNeutrons[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];
// extern const char* elem_table_element[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];
// extern const char* elem_table_symbol[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];
// extern const bool elem_table_Radioactive[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];
// extern const double
// elem_table_log_probability[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];

#include "Formula.hpp"


/////////////////////// Local includes
#include "globals.hpp"
#include "PeakCentroid.hpp"
#include <memory>
#include "IsotopicClusterGenerator.hpp"
#include "IsotopicDataLibraryHandler.hpp"
#include "IsotopicDataUserConfigHandler.hpp"
#include "IsotopicDataManualConfigHandler.hpp"
#include "pappsomspp/trace/trace.h"
#include "pappsomspp/types.h"


namespace msxps
{

namespace libmass
{


IsotopicClusterGenerator::IsotopicClusterGenerator()
{
}


IsotopicClusterGenerator::IsotopicClusterGenerator(
  libmass::IsotopicDataSPtr isotopic_data_sp)
  : msp_isotopicData(isotopic_data_sp)
{
}


IsotopicClusterGenerator::~IsotopicClusterGenerator()
{
  // qDebug();
}


void
IsotopicClusterGenerator::setIsotopicDataType(
  IsotopicDataType isotopic_data_type)
{
  m_isotopicDataType = isotopic_data_type;
}


void
IsotopicClusterGenerator::setIsotopicData(
  libmass::IsotopicDataSPtr isotopic_data_sp)
{
  msp_isotopicData = isotopic_data_sp;
}


libmass::IsotopicDataSPtr
IsotopicClusterGenerator::getIsotopicData() const
{
  return msp_isotopicData;
}


void
IsotopicClusterGenerator::setFormulaChargePair(
  FormulaChargePair &formula_charge_pair)
{
  m_formulaChargePairs.clear();
  m_formulaChargePairs.push_back(formula_charge_pair);

  // qDebug() << formula_charge_pair.first << formula_charge_pair.second;
}


void
IsotopicClusterGenerator::appendFormulaChargePair(
  FormulaChargePair &formula_charge_pair)
{
  m_formulaChargePairs.push_back(formula_charge_pair);

  // qDebug() << formula_charge_pair.first << formula_charge_pair.second;
}


void
IsotopicClusterGenerator::setFormulaChargePairs(
  const std::vector<FormulaChargePair> &formula_charge_pairs)
{
  m_formulaChargePairs.clear();

  m_formulaChargePairs.assign(formula_charge_pairs.begin(),
                              formula_charge_pairs.end());

  // qDebug() << "Set" << m_formulaChargePairs.size() << "formula/charge pairs";
  // for(auto pair : m_formulaChargePairs)
  // qDebug() << pair.first << "/" << pair.second;
}


void
IsotopicClusterGenerator::setMaxSummedProbability(double max_probability)
{
  m_maxSummedProbability = max_probability;
}


void
IsotopicClusterGenerator::setNormalizationIntensity(int normalize_intensity)
{
  m_normalizeIntensity = normalize_intensity;
}


void
IsotopicClusterGenerator::setSortType(pappso::SortType sort_type)
{
  m_sortType = sort_type;
}


void
IsotopicClusterGenerator::setSortOrder(pappso::SortOrder sort_order)
{
  m_sortOrder = sort_order;
}


bool
IsotopicClusterGenerator::validateFormula(Formula &formula)
{
  // qDebug() << "Checking formula:" << formula.toString()
  //<< "against an isotopic data set of " << msp_isotopicData->size()
  //<< "isotopes";

  // Check the syntax of the formula. Note that we need an obligatory count
  // index even for elements that are present a single time (H2O1 note the
  // 1).

  // IsoSpec requires that even single-count element be qualified with an
  // index (H2O1)

  formula.setForceCountIndex(true);

  // We have to validate because the formula might be "C5H6N3-O1", in which case
  // if would fail the simple checkSyntax() call (that call needs to be used on
  // already split parts of a formula).

  if(!formula.validate(
       msp_isotopicData, true /*store atom count*/, true /*reset counts*/))
    return false;

  return true;
}


bool
IsotopicClusterGenerator::validateAllFormulas()
{
  for(FormulaChargePair &pair : m_formulaChargePairs)
    {
      Formula formula(pair.first);

      if(!validateFormula(formula))
        return false;
    }

  return true;
}


pappso::TraceSPtr
IsotopicClusterGenerator::runIsotopicDataCalculations(
  std::size_t element_count,
  int charge,
  int *per_element_isotopes_count_array_p,
  int *per_element_symbol_count_array_p,
  double **per_element_isotope_masses_arrays_p_p,
  double **per_element_isotope_probs_arrays_p_p)
{
  // We get all the isotopic data relevant to the isotopic cluster modelling
  // calculation as performed by the IsoSpec library.

  if(per_element_isotopes_count_array_p == nullptr ||
     per_element_symbol_count_array_p == nullptr ||
     per_element_isotope_masses_arrays_p_p == nullptr ||
     per_element_isotope_probs_arrays_p_p == nullptr)
    qFatal("Programming error. The pointers cannot be nullptr.");


  if(m_maxSummedProbability <= 0 || m_maxSummedProbability > 1)
    {
      qDebug() << "The maximum summed probability has an incorrect value:"
               << m_maxSummedProbability;
      return nullptr;
    }

  IsoSpec::IsoLayeredGenerator iso(
    IsoSpec::Iso(element_count,
                 per_element_isotopes_count_array_p,
                 per_element_symbol_count_array_p,
                 per_element_isotope_masses_arrays_p_p,
                 per_element_isotope_probs_arrays_p_p),
    // The three values below are from the documentation (default values in the
    // constructor). We have added them 20230329 because we discovered that they
    // were needed on minGW64, otherwise we would experience crashes.
    1000,
    1000,
    true,
    m_maxSummedProbability);

  // qDebug() << "iso's mono peak mass:" << iso.getMonoisotopicPeakMass();

  // Each time we run a calculation, we do store the results in a new
  // IsotopicCluster.

  pappso::TraceSPtr isotopic_cluster_sp = std::make_shared<pappso::Trace>();

  // We store the results as std::vector<std::shared_ptr<libmass:PeakCentroid>>
  // because we'll want to sort the values according to the user's requirements.

  double effective_summed_probs = 0;

  // Iterate in all the cluster configurations and output all the ones that
  // summatively make a total probability <= to the probability set by the
  // user.

  /////////////// ATTENTION ////////////////
  // The loop below is tricky,

  while(iso.advanceToNextConfiguration())
    {
      double iso_prob = iso.prob();
      double iso_mz   = iso.mass() / charge;

      // qDebug() << "For current configuration (charge accounted for):" <<
      // iso_mz
      //<< iso_prob
      //<< "and effective_probs_sum : " << effective_summed_probs;

      // Create a peak centroid and store it (remark that we change the mass
      // of the ion into m/z because the user had set the charge corresponding
      // to the formula for which the isotopic cluster is being computed.

      isotopic_cluster_sp->push_back(pappso::DataPoint(iso_mz, iso_prob));

      // qDebug() << "Pushed back new peak centroid:" << iso_mz << "/" <<
      // iso_prob;

      // We do this increment at the end of the block. Indeed, if we had set up
      // at the top of the block, then, if the very first centroid had already a
      // prob > m_maxSummedProbability, then we would end up with an empty
      // cluster!

      effective_summed_probs += iso_prob;

      if(effective_summed_probs > m_maxSummedProbability)
        {
          // qDebug() << "Reached the max value: effective_summed_probs:"
          //<< effective_summed_probs
          //<< "and m_maxSummedProbability:" << m_maxSummedProbability
          //<< "BREAKING.";
          break;
        }
      else
        {
          // qDebug() << "Not yet reached the max value: effective_summed_probs
          // : "
          //<< effective_summed_probs
          //<< "and m_maxSummedProbability:" << m_maxSummedProbability;
        }
    }

  // Now perform the normalization to the Gaussian apex intensity value if
  // so is requested by the user For this we first need to find
  // what is the most intensity peak centroid. Then, we'll normalize against
  // it by dividing all intensity that that most intense value and
  // multiplying by the requested gaussian apex intensity value.

  //qDebug() << "Now asking for normalization.";

  // Just a debug check.
  //qDebug() << "Before normalizing, first data point:"
           //<< isotopic_cluster_sp->front().toString();

  normalizeIntensities(isotopic_cluster_sp);

  // Just a debug check.
  //qDebug() << "After normalizing, first data point:"
           //<< isotopic_cluster_sp->front().toString();

  // Now check if the user requests to kind of sorting of the PeakCentroid
  // instances.

  sortPeakCentroids(isotopic_cluster_sp);

  // qDebug() << "Now returning a cluster of size:" <<
  // isotopic_cluster_sp->size();

  return isotopic_cluster_sp;
}


IsotopicClusterChargePair
IsotopicClusterGenerator::generateIsotopicClusterCentroids(
  FormulaChargePair formula_charge_pair)
{
  // qDebug() << "Starting generation of isotopic cluster for formula:"
  //<< formula_charge_pair.first;

  Formula formula(formula_charge_pair.first);

  // The check will generate useful data inside the Formula!
  if(!validateFormula(formula))
    return std::pair(std::make_shared<const pappso::Trace>(), 0);

  // Use the correct handler!

  std::unique_ptr<IsotopicDataBaseHandler> isotopic_data_handler_up = nullptr;

  if(m_isotopicDataType == IsotopicDataType::LIBRARY_CONFIG)
    {
      isotopic_data_handler_up =
        std::make_unique<IsotopicDataLibraryHandler>(msp_isotopicData);
    }
  else if(m_isotopicDataType == IsotopicDataType::USER_CONFIG)
    {
      isotopic_data_handler_up =
        std::make_unique<IsotopicDataUserConfigHandler>(msp_isotopicData);
    }
  else if(m_isotopicDataType == IsotopicDataType::MANUAL_CONFIG)
    {
      isotopic_data_handler_up =
        std::make_unique<IsotopicDataManualConfigHandler>(msp_isotopicData);
    }
  else
    qFatal("Programming error. The isotopic data type is not correct.");


  // At this point we need to create the arrays exactly as we do in the user
  // manual config. So we need to know how many different chemical element
  // symbols we have in the formula.

  std::map<QString, int> symbol_count_map = formula.getSymbolCountMap();

  std::size_t element_count = symbol_count_map.size();

  // qDebug() << "Number of different symbols in the formula:" << element_count;

  if(!element_count)
    {
      qDebug() << "There is not a single element in the Formula.";
      return std::pair(std::make_shared<const pappso::Trace>(), 0);
    }

  // qDebug() << "The validated formula has a symbol/count map size:"
  //<< element_count;

  // We have to copy the symbol/count map obtained by validating
  // the formula into the isotopic data handler. That map is essential for the
  // crafting by the handler of the different IsoSpec arrays.

  // At this point, all the elements defined by the user have been completed and
  // we'll have to create the static arrays that are needed by IsoSpec.

  // We now need to construct the C arrays for IsoSpec. The arrays need to
  // be filled-in very accurately.

  // This array lists the number of isotopes that each element has.
  // Typically, C has 2, O has 3, P has 1...
  int *per_element_isotopes_count_array_p = nullptr;

  // This array list the count of atoms of each element.
  // Typically H20 will have 2 for H and 1 for O.
  int *per_element_symbol_count_array_p = nullptr;

  // These are arrays of arrays! Each array contains a new sub-array for each
  // symbol. The sub-array contains the isotopic masses (or probs) for one of
  // the element symbols. The sub-arrays are allocated by the handler below.
  double **per_element_isotope_masses_arrays_p_p = nullptr;
  double **per_element_isotope_probs_arrays_p_p  = nullptr;

  // We pass the array pointers by reference.
  if(!configureIsotopicData(symbol_count_map,
                            per_element_isotopes_count_array_p,
                            per_element_symbol_count_array_p,
                            per_element_isotope_masses_arrays_p_p,
                            per_element_isotope_probs_arrays_p_p))
    {
      qDebug() << "Failed to actually prepare the isotopic data tables for the "
                  "computation.";

      return std::pair(std::make_shared<const pappso::Trace>(), 0);
    }

  // At this point we have all the array needed to work.

  pappso::TraceSPtr isotopic_cluster_sp =
    runIsotopicDataCalculations(element_count,
                                formula_charge_pair.second,
                                per_element_isotopes_count_array_p,
                                per_element_symbol_count_array_p,
                                per_element_isotope_masses_arrays_p_p,
                                per_element_isotope_probs_arrays_p_p);

  // FIXME
  // To avoid a memory leak, we need to delete the mass and prob heap-allocated
  // arrays.

  // delete[] per_element_isotopes_count_array_p;
  // delete[] per_element_symbol_count_array_p;

  // for(std::size_t iter = 0; iter < element_count; ++iter)
  //{
  // delete[] per_element_isotope_masses_arrays_p_p[iter];
  // delete[] per_element_isotope_probs_arrays_p_p[iter];
  //}

  if(isotopic_cluster_sp == nullptr)
    qDebug() << "Failed to compute an isotopic cluster for formula:"
             << formula_charge_pair.first;

  // Just a debug check.
  //qDebug() << "After normalizing, first data point:"
           //<< isotopic_cluster_sp->front().toString();

  // qDebug() << "Done generating cluster for formula:"
  //<< formula_charge_pair.first;

  return std::pair(isotopic_cluster_sp, formula_charge_pair.second);
}


bool
IsotopicClusterGenerator::configureIsotopicData(
  std::map<QString, int> &symbol_count_map,
  int *&per_element_isotopes_count_array_p,
  int *&per_element_symbol_count_array_p,
  double **&per_element_isotope_masses_arrays_p_p,
  double **&per_element_isotope_probs_arrays_p_p)
{
  // Start by allocating the arrays we'll have to feed in this configuration
  // work.

  // However, we need to ensure that we actually have some stuff to work on.
  // That stuff is kind of a formula in the form of the std::map<QString, int>
  // m_symbolCountMap that pairs the symbols of the atoms in the formula and
  // the count for each symbol.

  if(!symbol_count_map.size())
    return false;

  // qDebug() << "The isotopic data have" << msp_isotopicData->size()
  //<< "isotopes";

  // Example used in the comments below: glucose, C6H12O6.

  // How many isotopes of each element symbols are there?
  // C:2, H:2, O:3
  per_element_isotopes_count_array_p = new int[symbol_count_map.size()];

  // How many atoms of each chemical element symbol are there?
  // C:6, H:12, O:6
  per_element_symbol_count_array_p = new int[symbol_count_map.size()];

  // Each subarray contains the mass of one of the isotopes for the element
  // symbol.
  // First array for C (two masses), second array for H (two masses), third
  // array for O (three masses).
  per_element_isotope_masses_arrays_p_p = new double *[symbol_count_map.size()];

  // Each subarray contains the prob of one of the isotopes for the element
  // symbol.
  // First array for C (two probs), second array for H (two probs), third
  // array for O (three probs).
  per_element_isotope_probs_arrays_p_p = new double *[symbol_count_map.size()];

  // Index that will allow to address the right slot in the arrays being
  // filled with isotopic data.
  int current_symbol_index = 0;

  for(auto item : symbol_count_map)
    {
      QString symbol   = item.first;
      int symbol_count = item.second;

      // qDebug() << "Iterating in symbol/count:" << symbol << "/" <<
      // symbol_count;

      // Immediately fill-in the symbol count value.
      per_element_symbol_count_array_p[current_symbol_index] = symbol_count;

      // Now get iterator bounding the isotopes for this symbol.

      std::pair<std::vector<IsotopeSPtr>::const_iterator,
                std::vector<IsotopeSPtr>::const_iterator>
        iter_pair = msp_isotopicData->getIsotopesBySymbol(symbol);

      // Handy shortcuts
      std::vector<IsotopeSPtr>::const_iterator iter     = iter_pair.first;
      std::vector<IsotopeSPtr>::const_iterator iter_end = iter_pair.second;

      std::size_t isotope_count = std::distance(iter, iter_end);

      // qDebug() << "For symbol:" << symbol << "there are:" << isotope_count
      //<< "isotopes";

      // Sanity check
      if(isotope_count != msp_isotopicData->getIsotopeCountBySymbol(symbol))
        qFatal(
          "Programming error. The is something wrong with the isotopic "
          "data.");

      // Fill-in the isotope count for the current symbol.
      per_element_isotopes_count_array_p[current_symbol_index] = isotope_count;

      // Fill-in the isotopes (mass/prob). For each element symbol in the
      // symbol/count map, we allocate a double array the size of the number
      // of isotopes so as to store the mass of each isotope (same for the
      // probs later). Once we have done that fill-in, we can set the address
      // of the array of the array of array below.

      // Allocate a double array for the masses and another one for the probs.

      double *masses_p = new double[isotope_count];
      double *probs_p  = new double[isotope_count];

      int current_isotope_index = 0;

      while(iter != iter_end)
        {
          IsotopeSPtr isotope_sp = *iter;

          // qDebug() << "For symbol" << symbol << "iterating with index"
          //<< current_isotope_index << "with Isotope *"
          //<< isotope_sp.get();

          masses_p[current_isotope_index] = isotope_sp->getMass();
          probs_p[current_isotope_index]  = isotope_sp->getProbability();

          // Increment the iterator
          ++iter;
          // Increment the isotope index so that we fill next array cell.
          ++current_isotope_index;
        }

      // At this time the masses and probs for the isotopes of current symbol
      // have been filled-in.

      per_element_isotope_masses_arrays_p_p[current_symbol_index] = masses_p;
      per_element_isotope_probs_arrays_p_p[current_symbol_index]  = probs_p;

      // We need to increment this index so as to address the right slot in
      // the arrays!
      ++current_symbol_index;
    }

  // At this point we have documented all the isotopic data required to
  // perform a calculation with IsoSpec.

  return true;
}


void
IsotopicClusterGenerator::normalizeIntensities(
  pappso::TraceSPtr &isotopic_cluster_sp)
{

  //qDebug() << "The normalize_intensity is:" << m_normalizeIntensity;

  // The most intense peak centroid's intensity value needs to be set to the
  // requested value and the same change ratio needs to be applied to all the
  // other peak centroids.

  // No normalization is asked for.
  if(m_normalizeIntensity == std::numeric_limits<int>::min())
    {
      //qDebug() << "No normalization was asked for. Skipping.";
      return;
    }

  if(!isotopic_cluster_sp->size())
    {
      qDebug() << "The isotopic cluster has not a single data point.";
      return;
    }
  // First get the most intense centroid.

  double max_found_intensity = 0;

  // qDebug() << "isotopic_cluster_sp->size():" << isotopic_cluster_sp->size();

  pappso::Trace::iterator vector_iterator = std::max_element(
    isotopic_cluster_sp->begin(),
    isotopic_cluster_sp->end(),
    [](const pappso::DataPoint first, const pappso::DataPoint second) {
      return first.y < second.y;
    });

  if(vector_iterator == isotopic_cluster_sp->end())
    qFatal("Programming error");

  max_found_intensity = (*vector_iterator).y;

  if(!max_found_intensity)
    qFatal("The maximum intensity of the whole isotopic cluster is 0.");

  //qDebug().noquote() << "Peak centroid with maximum intensity: "
                     //<< vector_iterator->toString();

  // Calculate the ratio between the final requested intensity and the greatest
  // intensity. That ratio will be multiplied to intensity of the each peak
  // centroid, which is why we call it a factor.

  double intensity_factor = m_normalizeIntensity / max_found_intensity;

  //qDebug() << "Will multiply this intensity factor to each data point's "
              //"intensity value:"
           //<< intensity_factor;

  // Just a debug check.
  //qDebug() << "Before normalizing, first data point:"
           //<< isotopic_cluster_sp->front().toString();

  std::for_each(
    isotopic_cluster_sp->begin(),
    isotopic_cluster_sp->end(),
    [intensity_factor](pappso::DataPoint &data_point) // modify in-place
    {
      // qDebug() << "Before normalization:" <<
      // data_point.toString();

      double normalized_intensity = data_point.y * intensity_factor;

      data_point.y = normalized_intensity;

      // qDebug() << "After normalization:" <<
      // data_point.toString();
    });

  // At this point the centroids' intensity have been normalized.

  // Just a debug check.
  //qDebug() << "After normalizing, first data point:"
           //<< isotopic_cluster_sp->front().toString();
}


void
IsotopicClusterGenerator::sortPeakCentroids(
  pappso::TraceSPtr &isotopic_cluster_sp)
{
  if(m_sortType == pappso::SortType::no_sort)
    return;

  return isotopic_cluster_sp->sort(m_sortType, m_sortOrder);
}


QString
IsotopicClusterGenerator::clusterToString(
  const pappso::TraceCstSPtr &isotopic_cluster_sp) const
{

  // Export the results as a string.  Note how we do export the relative
  // intensity. If there was normalization, that value was updated, otherwise it
  // had been initialized identical to the intensity upon creation of the
  // PeakCentroid instances in the vector.

  QString text;

  for(const pappso::DataPoint &dp : *isotopic_cluster_sp)
    {
      text += QString("%1 %2\n").arg(dp.x, 0, 'f', 30).arg(dp.y, 0, 'f', 30);
    }

  // qDebug().noquote() << "Cluster to string: " << text;

  return text;
}


QString
IsotopicClusterGenerator::clustersToString() const
{

  // Export the results as a string.  Note how we do export the relative
  // intensity. If there was normalization, that value was updated, otherwise it
  // had been initialized identical to the intensity upon creation of the
  // PeakCentroid instances in the vector.

  QString text;

  for(auto isotopic_cluster_charge_pair : m_isotopicClusterChargePairs)
    {
      text += clusterToString(isotopic_cluster_charge_pair.first);
    }

  // qDebug().noquote() << text;

  return text;
}


std::size_t
IsotopicClusterGenerator::run()
{
  // Iterate in all the formula/charge pairs and for each compute an isotopic
  // cluster.

  m_isotopicClusterChargePairs.clear();

  for(auto formula_charge_pair : m_formulaChargePairs)
    {
      IsotopicClusterChargePair pair =
        generateIsotopicClusterCentroids(formula_charge_pair);

      if(!pair.first->size())
        {
          qFatal("The isotopic cluster is empty for formula: %s",
                 formula_charge_pair.first.toLatin1().data());
        }

      m_isotopicClusterChargePairs.push_back(pair);
    }

  return m_isotopicClusterChargePairs.size();
}


const std::vector<IsotopicClusterChargePair> &
IsotopicClusterGenerator::getIsotopicClusterChargePairs() const
{
  return m_isotopicClusterChargePairs;
}

} // namespace libmass

} // namespace msxps

