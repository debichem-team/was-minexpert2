<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE chapter [

<!ENTITY % entities SYSTEM "entities.ent">
%entities;
]>

<chapter xml:id="chap_recording-data-exploration-discoveries" 
xmlns="http://docbook.org/ns/docbook" version="5.0"
xmlns:xlink="http://www.w3.org/1999/xlink"> 

<info>

	<title>Recording data exploration discoveries</title>

	<!--<keywordset>-->
		<!--</keywordset>-->

</info>


<para>

	When doing mass analysis work it is often desirable to store the painstakingly
	manually picked &mz; or &mr; values for later use.  &mineXp2; provides
	a flexible process allowing one to record the data exploration discoveries to a
	file, the clipboard, the console or any combination thereof.

</para>


<para>

	In order to record the innumerous analysis steps that make a data
	exploration session, the

	<menuchoice>

		<guimenu>File</guimenu>

		<guimenuitem>Analysis preferences...</guimenuitem>

		</menuchoice> menu will open a dialog window shown in <xref
		linkend="fig_minexpert-analysis-preferences-wnd"/>.

	</para>

	<figure xml:id="fig_minexpert-analysis-preferences-wnd">

		<title>Setting-up of the recording of the data exploration discoveries</title>

		<mediaobject>

			<imageobject role="fo">
				<imagedata fileref="print-minexpert-analysis-preferences-wnd.png" scale="70"/>
			</imageobject>
			<imageobject role="html">
				<imagedata fileref="web-minexpert-analysis-preferences-wnd.png"/>
			</imageobject>

			<caption><para>

					It is possible to configure the recording system to
					record to either the console, the clipboard, a file (in append
					mode or in overwrite mode) or any combination thereof.  The format
					of the string is defined using special characters (see text) and
					might be defined specifically for the three main graphs: TIC/XIC
					chromatogram, mass spectrum and drift spectrum.

				</para>
			</caption>

		</mediaobject>

	</figure>

	<para>

    In that window, the user can select the destination of the data analysis
    recording system: console, clipboard, file or any combination of the three.
    When selecting file recording, the user might specify if the recording would
    overwrite any preexisting file or, instead, append to that file.  Depending
    on the kind of plot where data exploration occurs, the format of the data to
    be recorded needs to change. Indeed, it would make no sense to record the
    charge <emphasis>z</emphasis> when exploring data in the <guilabel>Drift
    spectra</guilabel> window. This is why the text format of the data export
    needs to be defined for each one of the three kinds of plots: TIC/XIC
    chromatogram, mass spectrum or drift spectrum (<guilabel>Specialized
    formats</guilabel> group box).

	</para>


	<para>

    Before delving into the configuration intricacies, let us tell immediately
    how to trigger the recording of the exploration discoveries: using the
    <keycap>SPACE</keycap> bar in the composite plot widget containing the graph
    being analyzed.

	</para>


	<para>

		The format used to define the text string to be stored on console
		and/or in file can contain particular tokens as described below:</para>

	<itemizedlist>

		<listitem>

			<para>

				<guilabel>%f</guilabel> : mass spectrometry data file name.</para>

		</listitem>

		<listitem>

			<para>

				<guilabel>%s</guilabel> : sample name.</para>

		</listitem>

		<listitem>

			<para>

				<guilabel>%X</guilabel> : value on the x-axis of the graph (no unit).
				For a drift spectrum, that would be drift times in milliseconds, for a
				mass spectrum, that would be &mz; values, for a TIC/XIC chromatogram,
				that would be retention times in minutes.

			</para>

		</listitem>

		<listitem>

			<para>

				<guilabel>%Y</guilabel> : value on the y-axis of the graph (no unit). In
				all the graph plots, that would be intensities in any unit provided by
				the mass spectrometer (typically, counts).

			</para>

		</listitem>

		<listitem>

			<para>

				<guilabel>%x</guilabel> : delta value on the x-axis (when appropriate,
				no unit).

			</para>

		</listitem>

		<listitem>

			<para>

				<guilabel>%y</guilabel> : delta value on the y-axis (when appropriate,
				no unit).

			</para>

		</listitem>

		<listitem>

			<para>

				<guilabel>%I</guilabel> : TIC intensity after TIC integration over a graph
				range</para>

		</listitem>

		<listitem>

			<para>

				<guilabel>%b</guilabel> : x-axis range <emphasis>begin</emphasis> point
				for the computation (where applicable, for example for the TIC
				integration to a single value).

			</para>

		</listitem>

		<listitem>

			<para>

				<guilabel>%e</guilabel> : x-axis range <emphasis>end</emphasis> point
				for the computation (where applicable).

			</para>

		</listitem>

		<listitem>

			<para>

				<emphasis>For mass spec plot widgets:</emphasis></para>

			<itemizedlist>

				<listitem>

					<para>

						<guilabel>%z</guilabel> : charge	</para>

				</listitem>

				<listitem>

					<para>

						<guilabel>%M</guilabel> : &mr; as computed during deconvolution (see
						<xref linkend="chap_mass-spectral-deconvolutions"/>).

					</para>

				</listitem>

			</itemizedlist>

		</listitem>

	</itemizedlist>

	<note>

		<para>

      It is important to keep in mind that the <guilabel>%z</guilabel> and
      <guilabel>%M</guilabel> format strings can only work if the user is
      actually analyzing a mass spectrum and if the user has
      <emphasis>effectively</emphasis> performed a deconvolution operation that
      has allowed computing these two values. If the values are not available,
      the program shows <guilabel>nan</guilabel> (<quote>not a number</quote>)
      in the textual output upon hitting the <keycap>SPACE</keycap> bar (see
      below).

			Likewise, the <guilabel>%I</guilabel> format string will only hold
			meaningful data if an integration to a single TIC intensity value has been
			determined (see <xref
			linkend="fig_minexpert-mz-to-tic-int-integration-plot-widget-after"/>) for
			that specific data analysis record.

		</para>

		<para>

			The recording of data analysis steps works in any trace plot widget (not
			yet in the color map plot widgets) even if there are more than one trace
			in a given plot widget. In the case the plot widget has more than one
			trace and none or more than one is selected, the program will ask that a
			single trace be selected. If data are to be scrutinized for more traces,
			simply select each trace in turn and trigger the analysis stanza to be
			output using the <keycap>SPACE</keycap> bar each time.  

		</para>

	</note>	


	<para>

		Once configured, the format strings might be stored in a drop down
		box for later use. To that end, click onto the <guilabel>Add to
		history</guilabel> button while having the format text displayed in
		the text editor and it will be appended to the drop-down list. The list
		gets stored when the dialog window is closed and will be filled-up again
		when the program is restarted.

	</para>


	<para>

		As an example, if the user defined the following format string for a
		mass spectrum graph:</para>

	<literallayout>
		Mass spec. :
		mz = (%X, %Y) z = %z
		filename = %f
		date = 20161021
		session = 20161021
		mslevel = 1 msion = esi msanal = tof
		chrom = DEAE fraction = 25
		seq =  pos =     oxlevel = 0 pos =
		intensity =
		comment =
	</literallayout>

	<para>

		Then, a resulting data exploration stanza that would be recorded will
		look like this:</para>

	<literallayout>
		Mass Spec. :
		mz = (1051.8, 50863) z = 1
		filename = 20161017-rusconi-frac-25-deae-20160712.mzml
		date = 20161021
		session = 20161021
		mslevel = 1 msion = esi msanal = tof
		chrom = DEAE fraction = 25
		seq =  pos =     oxlevel = 0 pos =
		intensity =
		comment =
	</literallayout>

	<para>

		Interestingly, the user can define any kind of format, leaving
		fields available for later filling-in. This feature is of immense value
		when the analysis file is used later to fill-in a database for easy
		storage and interrogation of the exploration discoveries.  In this case, it
		would be useful to have the file opened in an editor and at each new
		stanza, edit the <guilabel>comment</guilabel> field if something needs to
		be commented, like the shape/intensity of a mass peak, for
		example.

	</para>


	<para>

		Note that the program closes the file each time a new stanza has
		been written.  This makes it possible to edit that file safely in
		between each stanza record.  Remember to force the editor to reload the
		file from disk after each exploration discovery recording.

	</para>


	<para>

    When the recording involves sending the analysis data to the console, the
    data are sent to it as text that is colored the same as the spectrum that
    was under scrutiny.

	</para>


	<para>

    When the mouse cursor has been placed at the proper location on the graph
    (with or without &lmb;-click-dragging, depending on the situation), the user
    hits the <keycap>SPACE</keycap> bar and the data analysis stanza is recorded
    to the selected destination(s): console, clipboard, file.

	</para>


</chapter>


