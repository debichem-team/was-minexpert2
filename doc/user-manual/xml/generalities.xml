<?xml version='1.0' encoding='utf-8' ?>
<!DOCTYPE chapter [

<!ENTITY % entities SYSTEM "entities.ent">
%entities;
]>

<chapter
  xml:id="chap_generalities" 
  xmlns="http://docbook.org/ns/docbook" version="5.0"
  xmlns:xlink="http://www.w3.org/1999/xlink">

<info>

  <title>Generalities</title>

  <keywordset>
    <keyword>general concepts</keyword>
    <keyword>combinations</keyword>
    <keyword>integrations</keyword>
    <keyword>building</keyword>
    <keyword>installation</keyword>
    <keyword>processing threads</keyword>
  </keywordset>

</info>

<para>In this chapter, I wish to introduce some general concepts around the
  &mineXp2; program and the way data elements are named in this manual and in
  the program.</para>

<para>A mass spectrometry experiment generally involves monitoring the &mz;
  value of analytes injected in the mass spectrometer along a certain time
  duration. The &mz; value of each detected analyte is recorded along with the
  corresponding signal intensity&nbsp;&int;, so that a mass spectrum is nothing but
  a series of &mzi; pairs recorded along the acquisition duration.  All along
  the acquisition, the precise moment at which a given analyte is detected
  (and its &mzi; pair is recorded), is called the retention time of that
  analyte (&rt;).  This retention time is not to be misunderstood as the drift
  time of that analyte in an ion mobility mass spectrometry experiment.
  </para>

  <sect1>

    <title>

      Citing the &mineXp2; software.

    </title>

    <para>

      Please, cite the software using the following citation:

      mineXpert2: Full-Depth Visualization and Exploration of MSn Mass
      Spectrometry Data. Olivier Langella and Filippo Rusconi.
      <emphasis>Journal of the American Society for Mass Spectrometry</emphasis>
      (check for pages, as this is a pub ahead of print reference). DOI: 10.1021/jasms.0c00402.

    </para>

  </sect1>

  <sect1>

    <title>General concepts and terminologies</title> 

    <para> Most generally, the mass spectrometer acquires an important number of
    spectra in, say, one second. But all these spectra are
    <emphasis>combined</emphasis> together, and, on the surface, the massist
    only sees a <quote>slow</quote> acquisition of 1&nbsp;spectrum per second.
    This apparent slow acquisition rate is configurable.  At the time of
    writing, generally 1&nbsp;spectrum per second is recorded on disk. So, say
    we record mass spectra for 5 minutes, we would have recorded
    (5*60)&nbsp;spectra. 
  </para>

</sect1>

<sect1>

  <title>Acquiring Mass Data Along Time: To Profile or Not To Profile?</title>

  <para>
    As a mass spectrometry user, the reader of this manual certainly has used mass
    spectrometers where mass spectra are acquired and stored in different ways:

    <itemizedlist>

      <listitem> 

        <para>Mass spectra are acquired and summed&mdash;the next to the
        previous&mdash;in such a manner that one is left, at the end of the
        acquisition, with a single spectrum of which the various peak
        intensities have been increasing all along the acquisition. Indeed,
        in this mode, each new spectrum is actually
        <emphasis><quote>combined</quote></emphasis> to the previously
        acquired ones. The resulting mass spectrum that is displayed on
        screen and that gets ultimately stored on disk is called a
        <emphasis>combined spectrum</emphasis>.  This is typically the way
        MALDI-TOF mass spectrometers are used when manually acquiring data
        from samples deposited onto sample plates. We refer to this kind of
        acquisition as an <quote>accumulation</quote> mode
          acquisition;</para>

      </listitem>

      <listitem>

        <para>Mass spectra are acquired and stored on disk as a single file
        containing all the spectra, appended one after the other. There is
        no combination of the spectra: each time a new spectrum is displayed
        on screen, that spectrum is appended to the
        file.<footnote><para>Although there certainly is spectrum
        combination going on in the guts of the software, because the
        system actually acquires much more spectra than is visible on
        screen and each newly displayed spectrum is actually the
        combination of many spectra acquired under the
        surface.</para></footnote> This is typically the case when mass
        spectra are acquired all along a chromatography run and is
          generally called a <emphasis><quote>profile</quote></emphasis>
          mode acquisition. Note that this profile mode acquisition must
          not be mistaken as the profile mass peak type that negates the
            centroid mass peak type.</para>

        </listitem>

      </itemizedlist>

    </para>

  </sect1>

  <sect1>

    <title>Mass Data Visualisation: To Combine or Not To Combine?</title>

    <para> In the previous section, we mentioned <emphasis>spectrum
    combination</emphasis> a number of times.  What does that mean, that
    spectra are <emphasis><quote>combined</quote></emphasis> together into
    a single <emphasis><quote>combined spectrum</quote></emphasis>? Say we
    have 200&nbsp;spectra that need to be combined together into a
    <emphasis>single</emphasis> spectrum that summatively represents the
      data of these 200&nbsp;spectra.</para>

    <para>First, a new spectrum would be allocated <emphasis>(result
    spectrum)</emphasis>, entirely empty at first. Then, the very first
    spectrum of the 200&nbsp;spectra is literally copied into that result
    spectrum. At this point the combination occurs, according to an
    iterative process that has the following steps:

    <itemizedlist>

      <listitem>

        <para>Pick the next spectrum of the 200-spectra dataset;</para>


        <orderedlist>

          <listitem>

            <para> Pick the first &mzi; pair of the currently iterated
              spectrum;</para>

          </listitem>

          <listitem>

            <para> Look up in the <emphasis>result spectrum</emphasis> if a
              &mz;&nbsp;value identical to the &mz;&nbsp;value of the current
              &mzi; pair is already present;</para>

          </listitem>

          <listitem>

            <para> If the &mz;&nbsp;value is found, increment its intensity by
              the intensity of the &mzi; pair;</para>

          </listitem>

          <listitem>

            <para> Else, if the &mz;&nbsp;value is not found, add the current
              &mzi; pair to the result spectrum;</para>

          </listitem>

          <listitem>

            <para> Iterate over all the remaining &mzi; pairs of the current
              spectrum and redo these steps.</para>

          </listitem>

        </orderedlist>

      </listitem>

      <listitem>

        <para>Iterate over all the 198&nbsp;remaining spectra of the dataset
          and do the steps above for each single iterated spectrum.</para>

      </listitem>

    </itemizedlist>

  </para>

  <para> At the end of the two nested loops above, the combined spectrum is
  still a single spectrum that represents&mdash;summatively&mdash;all the
  200 spectra. This whole process is very computing-intensive, in particular
  if:

  <itemizedlist>

    <listitem>

      <para> The &mz;&nbsp;range is large: there are lots of points in
      each spectrum, which means that for each new &mzi; pair we need
      to iterate in the long list of &mz;&nbsp;values that make the
        result spectrum;</para>

    </listitem>

    <listitem>

      <para> The resolving power of the mass spectrometer is high: there
        are many points per &mz;&nbsp;range unit.  </para>

    </listitem>

  </itemizedlist>

</para>

<para>When a profile mode acquisition is performed, the user gets an innumerable
number of distinct spectra, all appended to a single file.  These unitary
spectra are virtually unusable if an initial processing is not performed. This
initial processing of the spectra is called <emphasis><quote>total ion current
chromatogram calculation</quote></emphasis>.  What is it? Let's say that the
user has performed a profile mode mass spectrometry acquisition on the eluate of
a chromatography column.  Now, imagine that the spectrometer stores the mass
data at a rate of one spectrum per second and that the chromatography gradient
develops over 45&nbsp;min: there would be a total of (45 * 60)&nbsp;spectra in
that file. The question is: &mdash;<emphasis><quote>How  can we provide the user
with a data representation that might be both meaningful and useful to start
exploring the data?</quote></emphasis> The conventional way of doing so is to
load all the mass spectra and compute the <emphasis><quote>total ion current
chromatogram</quote></emphasis> (the TIC chromatogram).  The analogy with
chromatography is evident: the TIC chromatogram is the same as the UV
chromatogram unless optical density is not the physical property that is
measured over time; instead, the amount of ions that are detected in the mass
spectrometer is measured over time.  That amount is actually the sum of the
intensities of all the &mzi; pairs detected in each spectrum. When mass data are
acquired during a chromatography run, often, the total ion current chromatogram
mirrors (mimicks) the UV chromatogram<footnote><para>Unless eluted analytes do
absorb UV light but do not either desorb/desolvate or ionize, or
both.</para></footnote>.  For each retention time (RT) a TIC value is computed
by summing the intensities of all the &mzi; pairs detected at that specific
  RT.</para>

<para>How is this total ion current chromatogram computed? This is an
iterative process: from the first spectrum (retention time value
0&nbsp;s), to the second spectrum (retention time value 1&nbsp;s) up to
the last spectrum (retention time 45&nbsp;min), the program computes the
sum of the intensities of all the spectrum's &mzi; pairs.  That
computation ends up with a map that relates each RT value  with the
corresponding TIC value.  The TIC chromatogram is nothing but a plot of
the TIC values as a function of RT values.  In that sense, it is indeed a
  chromatogram.</para>

<para>&mineXp2; works exactly in this way. When mass spectrometry data are
loaded from a file, the TIC chromatogram is computed and displayed. This TIC
chromatogram serves as the basis for the mass data exploration, as described
in this manual.  The TIC chromatogram serves as the basis for spectral
combinations that can be performed in various ways, and not all formally
<emphasis>combinations</emphasis>, which is why I prefer the term
<emphasis><quote>integrations</quote></emphasis>. Some of these integrations
are described below:

<itemizedlist>

  <listitem>

    <para>Integrating data from the TIC chromatogram to a single mass
      spectrum;</para>

  </listitem>

  <listitem>

    <para>Integrating data from the TIC chromatogram to a single drift
      spectrum;</para>

  </listitem>

</itemizedlist>

  </para>

  <para>Note that the reverse actions are possible (and indeed necessary for a
  thorough data exploration): selecting a region of a mass spectrum and asking
  that the TIC chromatogram be reconstituted from there; or selecting a region
  of a drift spectrum and asking that the TIC chromatogram be reconstituted from
  there also.  Finally, integrations may, of course, be performed from a mass
    spectrum to a drift spectrum, and reverse.</para>

</sect1>

<sect1>

  <title>Examples of Various Mass Spectral Data Integrations</title>

  <para> In the sections below, the inner workings of &mineXp2; are described
  for some exemplary mass data integrations. For example, when doing ion
  mobility mass spectrometry data exploration, it is essential to be able to
  characterize most finely the drift time of each and any analyte.  Since each
  analyte is actually defined as one or more &mzi; pairs, it is essential to be
  able to ask questions like the following:

  <itemizedlist>

    <listitem>

      <para>What is the drift time of the ions below this mass
        peak?</para>

    </listitem>

    <listitem>

      <para>What are all the drift times of all the analytes going
        through the mobility cell for a given retention time range?</para>

    </listitem>

    <listitem>

      <para>What are all the ions that are responsible for this
        shoulder in the drift spectrum?</para>

    </listitem>

  </itemizedlist>

</para>

<sect2>

  <title>TIC -> MZ integration</title>

  <para>What computation does actually &mineXp2; do when a mass spectrum
  is computed starting from a TIC chromatogram region, say between
  retention time RT&nbsp;minute&nbsp;7 and
    RT&nbsp;minute&nbsp;8.5?</para>

  <orderedlist>

    <listitem>

      <para>List all the mass spectra that were acquired between RT&nbsp;7
        and RT&nbsp;8.5. In this <emphasis>spectral set</emphasis>, there
        might be many hundreds of spectra that match this criterion, if we
        think that, in ion mobility mass spectrometry,
          &ap;&thinsp;200&nbsp;spectra are acquired  and stored individually
          every second (I mean it, every 1&nbsp;s time lapse);</para>

      </listitem>

      <listitem>

        <para> Allocate a new empty spectrum&mdash;the
        <emphasis><quote>combined spectrum</quote></emphasis>&mdash;and
        copy into it without modification the first spectrum of the
          spectral set;</para>

      </listitem>

      <listitem xml:id="each-1-spectrum">

        <para> Go to the next spectrum of the spectral
          set and iterate into each &mzi; pair:</para>

        <itemizedlist>

          <listitem>

            <para> Check if the  &mz;&nbsp;value of the iterated pair is
            already present in the combined spectrum. If so, increment the
            combined spectrum's &mzi; pair's intensity value by the
            intensity of the iterated &mzi; pair's intensity. If not, simply
              copy the iterated &mzi; pair in the combined spectrum; </para>

          </listitem>

          <listitem>

            <para> Iterate over all the remaining &mzi; pairs and
              perform the same action as above.</para>

          </listitem>

        </itemizedlist>

      </listitem>

      <listitem>

        <para> Iterate over all the remaining spectra of the spectral set and
          perform step number&nbsp;<xref linkend="each-1-spectrum"/>.</para>

      </listitem>

    </orderedlist>

    <para> &mineXp2; then displays the combined spectrum.</para>

  </sect2>

  <sect2>

    <title>TIC -> DT integration</title>

    <para> What computation does &mineXp2; actually do when a drift
    spectrum is computed starting from a given TIC chromatogram region,
    say between retention time RT&nbsp;minute&nbsp;7 and
      RT&nbsp;minute&nbsp;8.5?</para> 

    <para> What is a drift spectrum? A drift spectrum (mobilogram) is a
    plot where the cumulated ion current of the detected ions is plotted
    against the drift time at which they were detected. Let's see how
      that computation is handled in &mineXp2;:

    <orderedlist>

      <listitem>

        <para>Create a map to store all the (drift time, intensity) pairs
          that are to be computed below, the (dt,i) map;</para>

      </listitem>

      <listitem>

        <para>List all the mass spectra that were acquired between
        RT&nbsp;7 and RT&nbsp;8.5.  The obtained list of mass spectra is
        called the <emphasis><quote>spectral
          set</quote></emphasis>;</para>

  </listitem>

  <listitem xml:id="each-2-spectrum">

    <para>Go to the first spectrum of the spectral set and compute its
    TIC value (sum of all the intensities of all the &mzi; pairs of
    that spectrum). Get the drift time value at which this mass
    spectrum was acquired. We thus have a value pair: (dt, i), that
    is, for drift time <emphasis>dt</emphasis>, the intensity of the
      total ion current is <emphasis>i</emphasis>;</para>

    <para> At this point, we need to do a short digression: we saw
    earlier that, at the time of this writing, one of the
    commercial instruments on which the author of these lines does
    his experiments stores 200&nbsp;spectra each second. These
    200&nbsp;spectra actually correspond to the way the drift
    cycle is divided into 200&nbsp;bin (time bins). That means
    that in the retention time range [7&ndash;8.5], there are
    (1.5*60) complete drift cycles. And thus there are (1.5*60)
    spectra with drift time&nbsp;x, the same amount of spectra
    with drift time&nbsp;y, and so on for the reminaing
    198&nbsp;time bins.  Of course, a large number of these
    spectra might be almost empty, but these spectra are there and
      we need to cope with them.</para>

    <para>The paragraph above must thus lead to one interrogation about
    the current (dt,i) pair: &mdash;<emphasis><quote>Has the current
    dt value be seen before, during the previous iterations in
    this loop?</quote></emphasis>. If not, then create the (dt, i)
    pair and add it to the (dt,i) map; if yes, get the
    <emphasis>dt</emphasis> element in the map and increment its
      intensity value by the TIC value computed above;</para>

  </listitem>

  <listitem>

    <para>Iterate over all the remaining spectra of the spectral set and
      perform step number&nbsp;<xref linkend="each-2-spectrum"/>.</para>

  </listitem>

</orderedlist>

      </para>

      <para> At the end of the loop above, we get a map in which each item
      relates a given drift time with a TIC value. This can be understood
      this way: &mdash;<emphasis><quote>For each drift time value, what is
      the accumulated ion current of all the ions having that specific
        drift time?</quote></emphasis>.</para>

  <para>At this point, &mineXp2; displays the drift spectrum
    (mobilogram).</para>

</sect2>

<sect2 xml:id="sec_integrations-using-multiple-threads">

  <title>Using multiple threads during mass data integrations</title>

  <para>

    Whenever the computer has multiple threads available for the integrations to
    be performed using parallel execution, &mineXp2; manages to use all these
    available threads.  It is possible to limit the number of threads used for
      the integration computations as described in <xref
        linkend="par_max-thread-count-preferences"/>, <xref
          linkend="sec_main-program-window-menu"/>.

      </para>

    </sect2>

  </sect1>


  <sect1 xml:id="sec_installation-build-software">

    <title>Installation of the software</title>

    <para>

        The installation material is available at <link xlink:href="http:/msxpertsuite.org"/>.

      </para>

      <sect2 xml:id="sec_installation-windows-macos-systems">

        <title>Installation on MS&nbsp;Windows and macOS systems</title>

        <para>

          The installation of the software is extremely easy on the MS-Windows and
          macOS platforms. In both cases, the installation programs are standard and
          require no explanation.

        </para>

      </sect2>


      <sect2 xml:id="sec_installation-debian-ubuntu-systems">

        <title>Installation on Debian- and Ubuntu-based systems</title>

        <para>

          The installation on Debian- and Ubuntu-based GNU/Linux platforms is
          also extremely easy (even more than in the above situations).
            &mineXp2; is indeed packaged and released in the official
          distribution repositories of these distributions and the only command
          to run to install it is:

        </para>

        <para>

          <prompt>$</prompt>

          <footnote><para>The prompt character might be
          <keysym>%</keysym> in some shells, like
          <application>zsh</application>.</para></footnote> 

      <command>sudo apt install &lt;package_name&gt;</command><keycap>RETURN</keycap>

    </para>

    <para>

      In the command above, the typical <emphasis>package_name</emphasis> is
      in the form <filename>minexpert2</filename> for the program package and
      <filename>minexpert2-doc</filename> for the user manual package.

    </para>

    <para>

      Once the package has been installed the program shows up in the
      <emphasis>Science</emphasis> menu. It can also be launched from the shell
      using the following command: 

    </para>

    <para>

      <prompt>$</prompt> <command>minexpert2</command><keycap>RETURN</keycap>

    </para>

    <tip>

      <para>

        If the Debian system onto which the program is to be installed is older
        than <emphasis>testing</emphasis>, that is, older than <emphasis>Buster
        (Debian&nbsp;10)</emphasis>, then using the AppImage program bundle might
        be a solution. See below for the method to run &mineXp2; as an AppImage
        bundle.

      </para>

    </tip>

  </sect2>

  <sect2 xml:id="sec_installation-with-appimage-software-bundles">

    <title>Installation with an AppImage software bundle</title>

    <para>

      The <emphasis>AppImage</emphasis> software bundle format is a format
      that allows one to easily run a software program on any GNU/Linux-based
        distribution. From the <link xlink:href="http:/appimage.org/"/>:

        <blockquote>

          <attribution>Simon Peter</attribution>

          <para>

            The key idea of the AppImage format is one app = one file. Every
            AppImage contains an app and all the files the app needs to run. In
            other words, each AppImage has no dependencies other than what is
            included in the targeted base operating system(s).  

          </para>

        </blockquote>

      </para>

      <para>

        There are AppImage software bundles for the various &mineXp2; versions
        that are available for download.  As of writing, the software bundle has
        been tested on <productname>Centos</productname> version&nbsp;8.3.2011
        and on <productname>Fedora</productname> version&nbsp;22. These are
        pretty old distribution versions and thus &mineXp2; should also run on
        more recent versions of these computing platforms. The AppImage bundle
        of &mineXp2; was created on a rather current Debian version: the
        <emphasis>testing</emphasis> <productname>Debian</productname> 11-to-be
        distribution.

      </para>

      <para>

        In order to run the &mineXp2; software AppImage bundle, download the
        latest version (like
        <filename>mineXpert2-0.7.4-x86_64.AppImage</filename>). Once the file
        has been downloaded to the desired directory, change to that directory
        and change the permissions to make it executable:

      </para>

      <para>

        <prompt>$</prompt> <command> chmod a+x
          mineXpert2-0.7.4-x86_64.AppImage</command><keycap>RETURN</keycap>

      </para>

      <para>

        Finally, execute the file that has become a normal program:

      </para>

      <para>

        <prompt>$</prompt> <command>
            ./mineXpert2-0.7.4-x86_64.AppImage</command><keycap>RETURN</keycap>

        </para>

        <tip>

          <para>

            If the program complains about a locale not being found, please, modify
            the command line to read:

          </para>

          <para>

            <prompt>$</prompt> <command>
                LC_ALL="C" ./mineXpert2-0.7.4-x86_64.AppImage</command><keycap>RETURN</keycap>

            </para>

          </tip>

        </sect2>

      </sect1>

      <sect1>

        <title>Building the software from source</title>

        <para>

          The &mineXp2; software build is under the control of the
          <application>CMake</application> build system.  There are a number of
          dependencies to install prior to trying to build the software, as
          described below.

        </para>

        <sect2>

          <title>The dependencies required to build &minexp2;</title>

          <para>

            The dependencies to be installed are listed here with package names
            matching the packages that are in <orgname>Debian/Ubuntu</orgname>. In
            other RPM-based software, most often the package names are similar, albeit
            with some slight differences. 

          </para>

          <variablelist><title>Dependencies</title>

            <varlistentry>

              <term>The build system</term>

              <listitem>
                <para>cmake</para>
              </listitem>

            </varlistentry>

            <varlistentry>

              <term>Conversion of <filename>svg</filename> files to
              <filename>png</filename> files</term>

            <listitem>
              <para>graphicsmagick-imagemagick-compat</para>
            </listitem>

          </varlistentry>

          <varlistentry>

            <term>For the parallel computations</term>

            <listitem>
              <para>libgomp1</para>
            </listitem>

          </varlistentry>

          <varlistentry>

            <term>For the isotopic cluster calculations</term>

            <listitem>
              <para>libisospec++-dev</para>
            </listitem>

          </varlistentry>

          <varlistentry>

            <term>For all the raw mass calculations like the data model, the mass
              spectral combinations…</term>

            <listitem> <para>libpappsomspp-dev, libpappsomspp-widget-dev</para>
            </listitem>

          </varlistentry>

          <varlistentry>

            <term>For all the plotting</term>

            <listitem>
              <para>libqcustomplot-dev</para>
            </listitem>

          </varlistentry>

          <varlistentry>

            <term>For the C++ objects (GUI and non-GUI)</term>

            <listitem> <para>qtbase5-dev, libqt5svg5-dev,
              qttools5-dev-tools, qtchooser</para> </listitem>

        </varlistentry>

        <varlistentry>

          <term>For the man page</term>

          <listitem>
            <para>docbook-to-man</para>
          </listitem>

        </varlistentry>

        <varlistentry>

          <term>For the documentation (optional, with
          <option>-DMAKE_USER_MANUAL=1</option> as a flag to the call of
          <application>cmake</application>, see below.)</term>

        <listitem> <para>daps, libjeuclid-core-java,
        libjeuclid-fop-java, libjs-jquery, libjs-highlight.js,
        libjs-mathjax, fonts-mathjax, fonts-mathjax-extras, texlive-fonts-extra,
          fonts-ebgaramond-extra</para> </listitem>

    </varlistentry>

  </variablelist>

</sect2>

<sect2>

  <title>Getting the source tarball</title>

  <para>

    In the example below, the version of the software to be installed is
    <parameter>7.3.0</parameter>.  Replace that version with any latest version
    of interest, which can be looked for at
    https://gitlab.com/msxpertsuite/minexpert2/-/releases.

  </para>

  <sect3>

    <title> Using git</title>

    <para>The rather convoluted command below only downloads the branch of
      interest. The whole git repos is very large&hellip;</para>

    <para>

      <prompt>$</prompt> <command> git clone
      https://gitlab.com/msxpertsuite/minexpert2.git --branch master/7.3.0-1
        --single-branch minexpert2-7.3.0</command>

    </para> 

  </sect3>

  <sect3>

    <title> Using wget to download the tarball</title>

    <para>

      <command>wget
        https://gitlab.com/msxpertsuite/minexpert2/-/archive/7.3.0/minexpert2-7.3.0.tar.gz</command>

    </para>

    <para>

      Untar the tarball, which creates the
      <filename>minexpert2-7.3.0</filename> directory:

    </para>

    <para> 

      <command>tar xvzf minexpert2-7.3.0.tar.gz</command>

    </para>

  </sect3>

</sect2>

<sect2>

  <title>Building of the software</title>

  <para>

    <literallayout language="shell">

      Change directory:

      <prompt>$</prompt> <command>cd minexpert2-7.3.0</command>

      Create a build directory:

      <prompt>$</prompt> <command>mkdir build</command>

      Change directory:

      <prompt>$</prompt> <command>cd build</command>

      Configure the build:

      <prompt>$</prompt> <command>cmake ../ -DCMAKE_BUILD_TYPE=Release</command>

      Build the software:

      <prompt>$</prompt> <command>make</command>

    </literallayout>

  </para>

</sect2>

</sect1>

  </chapter>

